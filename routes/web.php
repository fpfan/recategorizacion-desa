<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
	Route::get('cambiar_contrasena','UsuariosController@cambiar_contrasena')->name('cambiar_contrasena');
	Route::post('cambiar_pass','UsuariosController@cambiar_pass')->name('cambiar_pass');
});

Route::group(['middleware' => ['auth', 'activo']], function () {	
	Route::get('/getAgrupamientos/{id}', 'EmpleadoRecategorizacionController@getAgrupamientos')->name('getAgrupamientos');
	Route::get('/getCategorias/{id}', 'EmpleadoRecategorizacionController@getCategorias')->name('getCategorias');

	Route::post('/calcular', 'EmpleadoRecategorizacionController@calcular')->name('empleadoRecategorizacions.calcular');

	Route::get('/descargar_recategorizacion', 'EmpleadoController@descargar_recategorizacion')->name('descargar_recategorizacion');
	Route::get('/imprimir_recategorizacion', 'EmpleadoController@imprimir_recategorizacion')->name('imprimir_recategorizacion');

	Route::resource('empleadoRecategorizacions', 'EmpleadoRecategorizacionController');

	Route::resource('empleados', 'EmpleadoController');

});

Route::group(['middleware' => ['auth', 'activo', 'role:administrador']], function () {

	Route::resource('agrupamientos', 'AgrupamientoController');

	Route::resource('categorias', 'CategoriaController');

	Route::resource('regimens', 'RegimenController');

	Route::resource('localidades', 'LocalidadesController');

	Route::resource('zonas', 'ZonasController');

	Route::resource('zonaLocalidads', 'ZonaLocalidadController');

	Route::resource('titulos', 'TituloController');

	Route::resource('usuarios', 'UsuariosController');

	Route::resource('rols', 'RolController');

	Route::resource('roleUsers', 'Role_UserController');

	Route::resource('usuarioOps', 'UsuarioOpController');

	Route::resource('permisos', 'PermisoController');

	Route::resource('permisoRols', 'PermisoRolController');

	Route::get('buscar_usuario', 'UsuariosController@buscar_usuario')->name('buscar_usuario');
	Route::get('buscar_usuario_resetear', 'UsuariosController@buscar_usuario_resetear')->name('buscar_usuario_resetear');
	
	Route::get('resetear_contrasena/{id}', 'UsuariosController@resetear_contrasena')->name('resetear_contrasena');

	Route::resource('provincias', 'ProvinciaController');

});
