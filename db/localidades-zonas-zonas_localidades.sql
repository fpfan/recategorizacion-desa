-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-10-2019 a las 15:16:24
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pp_doc_11_10`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pp_localidades`
--

CREATE TABLE `pp_localidades` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pp_localidades`
--

INSERT INTO `pp_localidades` (`id`, `descripcion`, `id_provincia`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PENAS BLANCAS', 1, NULL, '2019-06-18 18:59:45', NULL),
(2, 'CATRIEL', 1, NULL, NULL, NULL),
(3, 'CINCO SALTOS', 1, NULL, NULL, NULL),
(4, 'CONTR. CORDERO', 1, NULL, NULL, NULL),
(5, 'CIPOLLETTI', 1, NULL, NULL, NULL),
(6, 'FERNANDEZ ORO', 1, NULL, NULL, NULL),
(7, 'ALLEN', 1, NULL, NULL, NULL),
(8, 'GENERAL ROCA', 1, NULL, NULL, NULL),
(9, 'CERVANTES', 1, NULL, NULL, NULL),
(10, 'MAINQUE', 1, NULL, NULL, NULL),
(11, 'ING. HUERGO', 1, NULL, NULL, NULL),
(12, 'GRAL. E. GODOY', 1, NULL, NULL, NULL),
(13, 'VILLA REGINA', 1, NULL, NULL, NULL),
(14, 'CHICHINALES', 1, NULL, NULL, NULL),
(15, 'VALLE AZUL', 1, NULL, NULL, NULL),
(16, 'CHELFORO', 1, NULL, NULL, NULL),
(17, 'LUIS BELTRAN', 1, NULL, NULL, NULL),
(18, 'LAMARQUE', 1, NULL, NULL, NULL),
(19, 'POMONA', 1, NULL, NULL, NULL),
(20, 'CHOELE CHOEL', 1, NULL, NULL, NULL),
(21, 'CHIMPAY', 1, NULL, NULL, NULL),
(22, 'CNEL. BELISLE', 1, NULL, NULL, NULL),
(23, 'DARWIN', 1, NULL, NULL, NULL),
(24, 'RIO COLORADO', 1, NULL, NULL, NULL),
(25, 'GENERAL CONESA', 1, NULL, NULL, NULL),
(26, 'GUARDIA MITRE', 1, NULL, NULL, NULL),
(27, 'MENCUE', 1, NULL, NULL, NULL),
(28, 'EL CUY', 1, NULL, NULL, NULL),
(29, 'LOS MENUCOS', 1, NULL, NULL, NULL),
(30, 'SIERRA COLORADA', 1, NULL, NULL, NULL),
(31, 'RAMOS MEXIA', 1, NULL, NULL, NULL),
(32, 'VALCHETA', 1, NULL, NULL, NULL),
(33, 'AGUADA CECILIO', 1, NULL, NULL, NULL),
(34, 'SAN ANTONIO OESTE', 1, NULL, NULL, NULL),
(35, 'LAS GRUTAS', 1, NULL, NULL, NULL),
(36, 'VIEDMA', 1, NULL, NULL, NULL),
(37, 'EL CONDOR', 1, NULL, NULL, NULL),
(38, 'SAN JAVIER', 1, NULL, NULL, NULL),
(39, 'WINTER', 1, NULL, NULL, NULL),
(40, 'BARILOCHE', 1, NULL, NULL, NULL),
(41, 'PILCANIYEU', 1, NULL, NULL, NULL),
(42, 'VILLA MASCARDI', 1, NULL, NULL, NULL),
(43, 'EL BOLSON', 1, NULL, NULL, NULL),
(44, 'NORQUINCO', 1, NULL, NULL, NULL),
(45, 'COMALLO', 1, NULL, NULL, NULL),
(46, 'ING. JACOBACCI', 1, NULL, NULL, NULL),
(47, 'RIO CHICO', 1, NULL, NULL, NULL),
(48, 'CLEMENTE ONELLI', 1, NULL, NULL, NULL),
(49, 'MAQUINCHAO', 1, NULL, NULL, NULL),
(50, 'EL CAIN', 1, NULL, NULL, NULL),
(51, 'CONA NIYEU', 1, NULL, NULL, NULL),
(52, 'SIERRA PAILEMAN', 1, NULL, NULL, NULL),
(53, 'LOS BERROS', 1, NULL, NULL, NULL),
(54, 'SIERRA GRANDE', 1, NULL, NULL, NULL),
(55, 'ARROYO VENTANA', 1, NULL, NULL, NULL),
(56, 'AGUADA DE GUERRA', 1, NULL, NULL, NULL),
(57, 'AGUADA GUZMAN', 1, NULL, NULL, NULL),
(58, 'CERRO POLICIA', 1, NULL, NULL, NULL),
(59, 'CUBANEA', 1, NULL, NULL, NULL),
(60, 'COMI CO', 1, NULL, NULL, NULL),
(61, 'EL MANSO', 1, NULL, NULL, NULL),
(62, 'FORTIN UNO', 1, NULL, NULL, NULL),
(63, 'JUAN DE GARAY', 1, NULL, NULL, NULL),
(64, 'LAS BAYAS', 1, NULL, NULL, NULL),
(65, 'LA JAPONESA', 1, NULL, NULL, NULL),
(66, 'MANUEL CHOIQUE', 1, NULL, NULL, NULL),
(67, 'NAHUEL NIYEU', 1, NULL, NULL, NULL),
(68, 'NAUPAHUEN', 1, NULL, NULL, NULL),
(69, 'OJO DE AGUA', 1, NULL, NULL, NULL),
(70, 'PASO FLORES', 1, NULL, NULL, NULL),
(71, 'PICHI MAHUIDA', 1, NULL, NULL, NULL),
(72, 'PILQUINIYEU', 1, NULL, NULL, NULL),
(73, 'ZANJON OYUELA', 1, NULL, NULL, NULL),
(74, 'VILLA MANZANO', 1, NULL, NULL, NULL),
(75, 'BARDA DEL MEDIO', 1, NULL, NULL, NULL),
(76, 'SARGENTO VIDAL', 1, NULL, NULL, NULL),
(77, 'J. J. GOMEZ', 1, NULL, NULL, NULL),
(78, 'VALLE VERDE', 1, NULL, NULL, NULL),
(79, 'PEN. RUCA CO', 1, NULL, NULL, NULL),
(80, 'VILLA S. EMILIO', 1, NULL, NULL, NULL),
(81, 'CAMPO GRANDE', 1, NULL, NULL, NULL),
(82, 'SAN ISIDRO', 1, NULL, NULL, NULL),
(83, 'PJE. LAS PERLAS', 1, NULL, NULL, NULL),
(84, 'LUTECIA', 1, NULL, NULL, NULL),
(85, 'PTO. SAN ANTONIO', 1, NULL, NULL, NULL),
(86, 'LOBERIA', 1, NULL, NULL, NULL),
(87, 'PJE.L.GUTIERREZ', 1, NULL, NULL, NULL),
(88, 'LLAO LLAO', 1, NULL, NULL, NULL),
(89, 'STEFENELLI', 1, NULL, NULL, NULL),
(90, 'CTE. M. GUERRICO', 1, NULL, NULL, NULL),
(91, 'DINA HUAPI', 1, NULL, NULL, NULL),
(92, 'MELIPAL', 1, NULL, NULL, NULL),
(93, 'CNIA.J.ECHARREN', 1, NULL, NULL, NULL),
(94, 'VILLA LLANQUIN', 1, NULL, NULL, NULL),
(95, 'FERRI', 1, NULL, NULL, NULL),
(96, 'PILQUINIYEU DEL LIMA', 1, NULL, NULL, NULL),
(100, 'EL FOYEL', 1, NULL, NULL, NULL),
(101, 'CHIPAUQUIL', 1, NULL, NULL, NULL),
(102, 'COLAN CONHUE', 1, NULL, NULL, NULL),
(103, 'LAGUNA BLANCA', 1, NULL, NULL, NULL),
(104, 'PRAHUANIYEU', 1, NULL, NULL, NULL),
(105, 'RINCON TRENETA', 1, NULL, NULL, NULL),
(106, 'YAMINUE', 1, NULL, NULL, NULL),
(110, 'ANECON GRANDE', 1, NULL, NULL, NULL),
(111, 'CERRO ALTO / CERRO EL PANTANOSO', 1, NULL, NULL, NULL),
(112, 'COLONIA CEFERINO NAMUNCURA', 1, NULL, NULL, NULL),
(113, 'COLONIA CONFLUENCIA', 1, NULL, NULL, NULL),
(114, 'COLONIA FATIMA', 1, NULL, NULL, NULL),
(115, 'COLONIA MARIA ELVIRA', 1, NULL, NULL, NULL),
(116, 'COLONIA SANTA GREGORIA', 1, NULL, NULL, NULL),
(117, 'COSTA DEL RIO AZUL', 1, NULL, NULL, NULL),
(118, 'CUATRO ESQUINAS', 1, NULL, NULL, NULL),
(119, 'FITA MICHE', 1, NULL, NULL, NULL),
(120, 'PARAJE EX-ISLA10', 1, NULL, NULL, NULL),
(121, 'PARAJE LA PARRA', 1, NULL, NULL, NULL),
(122, 'PARAJE SANTA LUCIA', 1, NULL, NULL, NULL),
(123, 'PICHI LEUFU', 1, NULL, NULL, NULL),
(124, 'PLAYAS DORADAS', 1, NULL, NULL, NULL),
(125, 'RINCON DE LAS PERLAS', 1, NULL, NULL, NULL),
(126, 'RIO VILLEGAS', 1, NULL, NULL, NULL),
(127, 'VILLA SAN ISIDRO', 1, NULL, NULL, NULL),
(128, 'CAÑADON CHILENO', 1, NULL, '2019-06-25 17:28:17', NULL),
(129, 'CORRALITO', 1, NULL, NULL, NULL),
(130, 'EL DIQUE', 1, NULL, NULL, NULL),
(131, 'EL JUNCAL', 1, NULL, NULL, NULL),
(501, 'BAHIA BLANCA', 2, NULL, NULL, NULL),
(507, 'CARMEN DE PATAGONES', 2, NULL, NULL, NULL),
(999, 'CAPITAL FEDERAL', 2, NULL, NULL, NULL),
(1000, 'Neuquén', 3, NULL, NULL, NULL),
(1001, 'Afuera de RN', 6, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pp_zonas`
--

CREATE TABLE `pp_zonas` (
  `id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pp_zonas`
--

INSERT INTO `pp_zonas` (`id`, `codigo`, `descripcion`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'ZONA ATLANTICA', NULL, NULL, NULL),
(2, 2, 'ZONA ALTO VALLE', NULL, NULL, NULL),
(3, 3, 'ZONA VALLE MEDIO', NULL, NULL, NULL),
(4, 4, 'ZONA CORDILLERANA', NULL, NULL, NULL),
(5, 5, 'LINEA SUR', NULL, NULL, NULL),
(6, 6, 'FUERA DE RIO NEGRO', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pp_zona_localidad`
--

CREATE TABLE `pp_zona_localidad` (
  `id` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  `id_localidad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pp_zona_localidad`
--

INSERT INTO `pp_zona_localidad` (`id`, `id_zona`, `id_localidad`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 25, NULL, NULL, NULL),
(2, 1, 26, NULL, NULL, NULL),
(3, 1, 34, NULL, NULL, NULL),
(4, 1, 35, NULL, NULL, NULL),
(5, 1, 36, NULL, NULL, NULL),
(6, 1, 38, NULL, NULL, NULL),
(7, 1, 130, NULL, NULL, NULL),
(8, 1, 131, NULL, NULL, NULL),
(9, 2, 2, NULL, NULL, NULL),
(10, 2, 3, NULL, NULL, NULL),
(11, 2, 4, NULL, NULL, NULL),
(12, 2, 5, NULL, NULL, NULL),
(13, 2, 74, NULL, NULL, NULL),
(14, 2, 75, NULL, NULL, NULL),
(15, 2, 113, NULL, NULL, NULL),
(16, 2, 115, NULL, NULL, NULL),
(17, 2, 118, NULL, NULL, NULL),
(18, 2, 125, NULL, NULL, NULL),
(19, 2, 6, NULL, NULL, NULL),
(20, 2, 7, NULL, NULL, NULL),
(21, 2, 90, NULL, NULL, NULL),
(22, 2, 8, NULL, NULL, NULL),
(23, 2, 9, NULL, NULL, NULL),
(24, 2, 10, NULL, NULL, NULL),
(25, 2, 28, NULL, NULL, NULL),
(26, 2, 58, NULL, NULL, NULL),
(27, 2, 114, NULL, NULL, NULL),
(28, 2, 11, NULL, NULL, NULL),
(29, 2, 12, NULL, NULL, NULL),
(30, 2, 13, NULL, NULL, NULL),
(31, 2, 14, NULL, NULL, NULL),
(32, 2, 15, NULL, NULL, NULL),
(33, 2, 16, NULL, NULL, NULL),
(34, 2, 112, NULL, NULL, NULL),
(35, 3, 17, NULL, NULL, NULL),
(36, 3, 18, NULL, NULL, NULL),
(37, 3, 19, NULL, NULL, NULL),
(38, 3, 20, NULL, NULL, NULL),
(39, 3, 23, NULL, NULL, NULL),
(40, 3, 21, NULL, NULL, NULL),
(41, 3, 22, NULL, NULL, NULL),
(42, 3, 116, NULL, NULL, NULL),
(43, 3, 24, NULL, NULL, NULL),
(44, 3, 93, NULL, NULL, NULL),
(45, 4, 40, NULL, NULL, NULL),
(46, 4, 41, NULL, NULL, NULL),
(47, 4, 91, NULL, NULL, NULL),
(48, 4, 111, NULL, NULL, NULL),
(49, 4, 129, NULL, NULL, NULL),
(50, 4, 43, NULL, NULL, NULL),
(51, 4, 44, NULL, NULL, NULL),
(52, 4, 126, NULL, NULL, NULL),
(53, 5, 29, NULL, NULL, NULL),
(54, 5, 104, NULL, NULL, NULL),
(55, 5, 30, NULL, NULL, NULL),
(56, 5, 31, NULL, NULL, NULL),
(57, 5, 32, NULL, NULL, NULL),
(58, 5, 45, NULL, NULL, NULL),
(59, 5, 46, NULL, NULL, NULL),
(60, 5, 72, NULL, NULL, NULL),
(61, 5, 103, NULL, NULL, NULL),
(62, 5, 110, NULL, NULL, NULL),
(63, 5, 123, NULL, NULL, NULL),
(64, 5, 128, NULL, NULL, NULL),
(65, 5, 49, NULL, NULL, NULL),
(66, 5, 54, NULL, NULL, NULL),
(67, 5, 55, NULL, NULL, NULL),
(68, 5, 124, NULL, NULL, NULL),
(69, 6, 501, '2019-09-11 18:35:56', '2019-09-11 18:35:56', NULL),
(70, 6, 999, '2019-09-11 18:36:05', '2019-09-11 18:36:05', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pp_localidades`
--
ALTER TABLE `pp_localidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pp_zonas`
--
ALTER TABLE `pp_zonas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pp_zona_localidad`
--
ALTER TABLE `pp_zona_localidad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_zonaloc_1` (`id_localidad`),
  ADD KEY `fk_zonaloc_2` (`id_zona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pp_localidades`
--
ALTER TABLE `pp_localidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;

--
-- AUTO_INCREMENT de la tabla `pp_zonas`
--
ALTER TABLE `pp_zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `pp_zona_localidad`
--
ALTER TABLE `pp_zona_localidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pp_zona_localidad`
--
ALTER TABLE `pp_zona_localidad`
  ADD CONSTRAINT `fk_zonaloc_1` FOREIGN KEY (`id_localidad`) REFERENCES `pp_localidades` (`id`),
  ADD CONSTRAINT `fk_zonaloc_2` FOREIGN KEY (`id_zona`) REFERENCES `pp_zonas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
