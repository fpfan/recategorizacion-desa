<!-- Id Regimen Field -->
<div class="form-group">
    {!! Form::label('id_regimen', 'Id Regimen:') !!}
    <p>{!! $agrupamiento->id_regimen !!}</p>
</div>

<!-- Cod Reg Field -->
<div class="form-group">
    {!! Form::label('cod_reg', 'Cod Reg:') !!}
    <p>{!! $agrupamiento->cod_reg !!}</p>
</div>

<!-- Cod Agrup Field -->
<div class="form-group">
    {!! Form::label('cod_agrup', 'Cod Agrup:') !!}
    <p>{!! $agrupamiento->cod_agrup !!}</p>
</div>

<!-- Desc Agrup Field -->
<div class="form-group">
    {!! Form::label('desc_agrup', 'Desc Agrup:') !!}
    <p>{!! $agrupamiento->desc_agrup !!}</p>
</div>

