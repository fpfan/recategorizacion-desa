<!-- Id Regimen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_regimen', 'Id Regimen:') !!}
    {!! Form::number('id_regimen', null, ['class' => 'form-control']) !!}
</div>

<!-- Cod Reg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cod_reg', 'Cod Reg:') !!}
    {!! Form::number('cod_reg', null, ['class' => 'form-control']) !!}
</div>

<!-- Cod Agrup Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cod_agrup', 'Cod Agrup:') !!}
    {!! Form::number('cod_agrup', null, ['class' => 'form-control']) !!}
</div>

<!-- Desc Agrup Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desc_agrup', 'Desc Agrup:') !!}
    {!! Form::text('desc_agrup', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('agrupamientos.index') !!}" class="btn btn-default">Cancel</a>
</div>
