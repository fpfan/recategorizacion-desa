<div class="table-responsive">
    <table class="table" id="agrupamientos-table">
        <thead>
            <tr>
                <th>Id Regimen</th>
        <th>Cod Reg</th>
        <th>Cod Agrup</th>
        <th>Desc Agrup</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($agrupamientos as $agrupamiento)
            <tr>
                <td>{!! $agrupamiento->id_regimen !!}</td>
            <td>{!! $agrupamiento->cod_reg !!}</td>
            <td>{!! $agrupamiento->cod_agrup !!}</td>
            <td>{!! $agrupamiento->desc_agrup !!}</td>
                <td>
                    {!! Form::open(['route' => ['agrupamientos.destroy', $agrupamiento->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('agrupamientos.show', [$agrupamiento->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('agrupamientos.edit', [$agrupamiento->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
