<form action="{!! route('buscar_usuario') !!}" method="GET" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="busqueda" class="form-control" placeholder="Buscar por documento o nombre y apellido"/ style="background-color: white">
          <span class="input-group-btn">
            <button type='submit' id='search-btn' class="btn btn-flat" style="background-color: white"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form>


<div class="table table-responsive">
    <table class="table" id="usuarios-table">
        <thead>
            <tr>
                <th>Usuario</th>
                <th>Nro Dni</th>
                <th>Nombre y Apellido</th>
                <th>Email</th>
                <th>Rol</th>
                <th>Activo</th>
            </tr>
        </thead>
        <tbody>
        @foreach($usuarios as $usuario)
            <tr>
                <td>{!! $usuario->usuario !!}</td>
                <td>{!! $usuario->nro_dni !!}</td>
                <td>{!! $usuario->name !!}</td>
                <td>{!! $usuario->email !!}</td>
                @if ($usuario->role != NULL)
                <td>{!! $usuario->role->name !!}
                @else
                <td></td>
                @endif
                <td>@if($usuario->activo == false)
                 Inactivo
                 @else
                 Activo
                 @endif
                </td>

                <td>
                    <a href="{!! route('usuarios.edit', [$usuario->id]) !!}" class='btn btn-block btn-primary btn-xs'>Editar</a>
                    {!! Form::open(['route' => ['usuarios.destroy', $usuario->id], 'method' => 'delete']) !!}
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-block btn btn-danger btn-xs', 'onclick' => "return confirm('¿Está seguro?')"]) !!}
                    {!! Form::close() !!}

                    {!! Form::open(['route' => ['resetear_contrasena', $usuario->id], 'method' => 'get']) !!}
                    {!! Form::button('Resetear', ['type' => 'submit', 'class' => 'btn btn-block btn btn-warning btn-xs', 'onclick' => "return confirm('¿Está seguro de reestablecer la contraseña?')"]) !!}
                            {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>

{!! $usuarios->appends(['busqueda'=>$busqueda])->links() !!}