@extends('layouts.app')

@section('content')
@include('flash::message')
    <section class="content-header">
        <h1>
            Cambiar contraseña
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">

{!! Form::open(array('url' => 'cambiar_pass')) !!}
						<div class="form-group col-sm-12">
							<div class="form-group col-sm-4">
			    				{!! Form::label('password_viejo', 'Contraseña actual:') !!}
			    				{!! Form::password('password_viejo', ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group col-sm-12">
							<div class="form-group col-sm-4">
			    				{!! Form::label('password_nuevo', 'Nueva contraseña:') !!}
			    				{!! Form::password('password_nuevo', ['class' => 'form-control']) !!}
							</div>
						</div>
						
						<div class="form-group col-sm-12">
							<div class="form-group col-sm-4">
			    				{!! Form::label('confirmar_password', 'Confirmar contraseña:') !!}
			    				{!! Form::password('password_repetido', ['class' => 'form-control']) !!}
							</div>
						</div>
						<!-- Submit Field -->
						<div class="form-group col-sm-4 text-center">
						<br>
	    					{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
	    					<a href="{!! route('home') !!}" class="btn btn-default">Cancelar</a>
						</div>
            {!! Form::close() !!}
				</div>
            </div>
        </div>
    </div>
@endsection

