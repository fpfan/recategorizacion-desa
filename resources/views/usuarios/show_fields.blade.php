<h4><b>{{ $usuario->usuario }}</b></h4>

<!-- Name Field -->
<div class="form-group col-sm-4">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{!! $usuario->name !!}</p>
</div>

<!-- Nro Dni Field -->
<div class="form-group col-sm-4">
    {!! Form::label('nro_dni', 'Tipo y Nro de Documento:') !!}
    <p>{!! $usuario->tipo_dni->tipo !!}: {!! $usuario->nro_dni !!}</p>
</div>

<!-- Email Field -->
<div class="form-group col-sm-4">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $usuario->email !!}</p>
</div>

@if($usuario->op != null)
<!-- Id Op Cobra Field -->
<div class="form-group col-sm-4">
    {!! Form::label('id_op_cobra', 'Id Op Cobra:') !!}
    <p>{!! $usuario->op->descripcion !!}</p>
</div>
@endif

@if($usuario->role != null)
<!-- Id Op Cobra Field -->
<div class="form-group col-sm-8">
    {!! Form::label('id_rol', 'Rol:') !!}
    <p>{!! $usuario->role->name !!}</p>
</div>
@else
<div class="form-group col-sm-4">
    {!! Form::label('id_rol', 'Rol:') !!}
    <p>{!! $usuario->id_rol !!}</p>
</div>
@endif

<hr>

<div class="form-group col-sm-12">

@if($usuario->activo == false)
    <b>Activo: </b> No
@else
    <b>Activo: </b> Si
@endif
<br>
@if($usuario->habilitado == false)
    <b>Habilitado: </b> No
@else
    <b>Habilitado: </b> Si
@endif
</div>

<div class="form-group col-sm-12">
    @if($usuario->activo == false)
        <p><b>Usuario inactivo:</b> El usuario aún no se ha logueado por primera vez o se le ha hecho un reseteo de la contraseña. El usuario solo tendrá permiso para loguearse y cambiar la contraseña.</p>
    @elseif($usuario->habilitado == false)
        <p><b>Usuario Inhabilitado:</b> Los permisos de creación/edición y eliminación que tenga el rol serán revocados para el usuario. Solo tendrá permiso de Lectura.</p>
    @else
        <p><b>Usuario Habilitado: </b>El usuario tiene todos los permisos del rol.</p>         
    @endif
</div>

<div class="form-group col-sm-12">

<h4><b>Ordenes de Pago asociadas  </b><a href="{!! route('usuarioOps.edit', [$usuario->id]) !!}" class='btn btn-primary  btn-xs'>Editar</a></h4>
</div>

<div class="form-group col-sm-12">
    @foreach($usuario_ops as $uop)    
    <div class="form-group col-sm-4">
        {{$uop->id_op}} - {{ $uop->op->descripcion}}
    </div>
    @endforeach

</div>


