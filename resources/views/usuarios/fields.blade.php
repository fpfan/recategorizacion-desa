<!-- Name Field -->
@if($btn =='crear')
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control', 'maxlength' => '90', 'required' => '']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('apellido', 'Apellido:') !!}
    {!! Form::text('apellido', null, ['class' => 'form-control', 'maxlength' => '100', 'required' => '']) !!}
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre y Apellido:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => '']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('usuario', 'Usuario:') !!}
    {!! Form::text('usuario', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => '']) !!}
</div>
@endif

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191']) !!}
</div>

@if($btn =='crear')
<!-- Password Field -->
    {!! Form::hidden('password', 12345678, ['class' => 'form-control']) !!}
    {!! Form::hidden('activo', 0, ['class' => 'form-control']) !!}
@else
    {!! Form::hidden('password', $usuarios->password, ['class' => 'form-control']) !!}
    {!! Form::hidden('activo', $usuarios->activo, ['class' => 'form-control']) !!}
@endif

<!-- Nro Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nro_dni', 'Nro Documento:') !!}
    {!! Form::number('nro_dni', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Tipo Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_rol', 'Rol:') !!}
    {!! Form::select('id_rol', $roles, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('usuarios.index') !!}" class="btn btn-default">Cancelar</a>
</div>
