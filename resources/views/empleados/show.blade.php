@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left" style="text-transform: uppercase;">{{$empleadoRecategorizacion->Empleado->apynom}}</h1>
        <h1 class="pull-right">
            {!! Form::open(['route' => ['empleados.destroy', $empleadoRecategorizacion->id_empleado], 'method' => 'delete']) !!}
            <button type="submit" class="btn btn-danger pull-right" style="margin-top: -10px;
            margin-bottom: 5px; margin-left: 5px;" onclick = "return confirm('Esta Seguro?')">Eliminar</button>
            {!! Form::close() !!}
        </h1>
        <h1 class="pull-right">
            <form action="{!! route('descargar_recategorizacion') !!}" method="GET" target="_blank">
                <input type="hidden" name="id_emp_rec" id="id_emp_rec" value="{{$empleadoRecategorizacion->id}}">
                <button type="submit" class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-left: 5px;">Descargar</button>
            </form>
        </h1>
        <h1 class="pull-right">
            <form action="{!! route('imprimir_recategorizacion') !!}" method="GET" target="_blank">
                <input type="hidden" name="id_emp_rec" id="id_emp_rec" value="{{$empleadoRecategorizacion->id}}">
                <button type="submit" class="btn btn-info pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-left: 5px;">Imprimir</button>
            </form>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('empleados.show_fields')
                    <a href="{!! route('empleadoRecategorizacions.index') !!}" class="btn btn-default">Volver</a>
                     
                </div>
            </div>
        </div>
    </div>
@endsection
