
                        {!! Form::hidden('id_categoria_inicial', $empleadoRecategorizacion['id_categoria_inicial'], ['class' => 'form-control']) !!}

                        {!! Form::hidden('id_categoria_ascenso', $empleadoRecategorizacion['id_categoria_ascenso'], ['class' => 'form-control', 'readonly' => '']) !!}

                        {!! Form::hidden('fecha_inicial', $empleadoRecategorizacion['fecha_inicial']) !!}

                        {!! Form::hidden('fecha_ascenso', $empleadoRecategorizacion['fecha_ascenso']) !!}

                        {!! Form::hidden('fecha_final', $empleadoRecategorizacion['fecha_final']) !!}

                        @if(isset($empleadoRecategorizacion['id_localidad_trabaja']))
                        {!! Form::hidden('id_localidad_trabaja', $empleadoRecategorizacion['id_localidad_trabaja'], ['class' => 'form-control', 'readonly' => '']) !!}
                        @else
                        {!! Form::hidden('id_localidad_trabaja', null, ['class' => 'form-control', 'readonly' => '']) !!}
                        @endif


                        @if(isset($empleadoRecategorizacion['titulo']))
                        {!! Form::hidden('id_titulo', $empleadoRecategorizacion['id_titulo'], ['class' => 'form-control', 'readonly' => '']) !!}
                        @else
                        {!! Form::hidden('id_titulo', null, ['class' => 'form-control', 'readonly' => '']) !!}
                        @endif

                        @if(isset($empleadoRecategorizacion['tiene_capacitacion']))
                        {!! Form::hidden('tiene_capacitacion', $empleadoRecategorizacion['tiene_capacitacion'], ['class' => 'form-control', 'readonly' => '']) !!}
                        @else
                        {!! Form::hidden('tiene_capacitacion', null, ['class' => 'form-control', 'readonly' => '']) !!}
                        @endif


<div class="col-sm-12">
    <h3>Datos del Empleado</h3>
</div>
<!-- Apynom Field -->
<div class="form-group col-sm-6"> 
    {!! Form::label('apynom', 'Apellido y Nombres:') !!}
    {!! Form::text('apynom', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Nro Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nro_dni', 'Nro de Documento:') !!}
    {!! Form::number('nro_dni', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Nro Cuil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nro_expediente', 'Nro de  Expediente:') !!}
    {!! Form::text('nro_expediente', null, ['class' => 'form-control']) !!}
</div>

<!-- Nro Legajo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nro_legajo', 'Nro de Legajo:') !!}
    {!! Form::number('nro_legajo', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    <hr>
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('empleados.index') !!}" class="btn btn-default">Cancelar</a>
</div>
