<!DOCTYPE html>
<html>
<head>
	<title>Recategorización N° {{$recategorizacion->id}}</title>

	<style>
    	body{
     		font-size: 12pt; 
     		font-family: 'Times New Roman', Times;
     		margin: 70px;
    	}
    	#customers {
		  width: 100%;
		  font-size: 10pt;
		}
		#customers td, #customers th {
		  border: 1px solid #ddd;
		}
		#customers th {
		  text-align: left;
		  background-color: lightgrey;
		}
		.der{
		    text-align: right;
		}
		.izq{
			text-align: left;
		}
		.center{
			text-align: center;
		}
		.recuadro{
			border: 1px lightgrey solid;
			padding: 20px;
		}
	</style>
</head>
<body>
	<div class="recuadro">
		<h3 style=" text-transform: uppercase;">{{$recategorizacion->Empleado->apynom}}</h3>
		
		<table id="customers">
			<tr>
				<th style="width: 40%">NRO DE DOCUMENTO</th>
				<td>{{$recategorizacion->Empleado->nro_dni}}</td>
			</tr>
			<tr>
				<th style="width: 40%">NRO DE EXPEDIENTE</th>
				<td>{{$recategorizacion->Empleado->nro_expediente}}</td>
			</tr>
			<tr>
				<th style="width: 40%">NRO DE LEGAJO</th>
				<td>{{$recategorizacion->Empleado->nro_legajo}}</td>
			</tr>
			@if($recategorizacion->Categoria_inicial->Agrupamiento->Regimen->id == 2)
				@if($recategorizacion->Categoria_inicial->Agrupamiento->id == 3 OR $recategorizacion->Categoria_inicial->Agrupamiento->id == 6)
				<tr>
					<th style="width: 40%">TITULO</th>
					<td>{{$recategorizacion->Titulo->descripcion}}</td>
				</tr>
				@endif
			
				<tr>
					<th style="width: 40%">TIENE CAPACITACIÓN</th>
					<td>@if($recategorizacion->tiene_capacitacion) Si @else No @endif</td>
				</tr>
			@endif
			@if($recategorizacion->Categoria_inicial->Agrupamiento->Regimen->id == 3)
			<tr>
				<th style="width: 40%">LOCALIDAD TRABAJA</th>
				<td>{{$recategorizacion->Localidad->descripcion}}</td>
			</tr>
			@endif			
		</table>
		<br>
		<table id="customers">
			<tr>
				<th style="width: 40%">RÉGIMEN LABORAL</th>
				<td>{{$recategorizacion->Categoria_inicial->Agrupamiento->Regimen->descripcion}}</td>
			</tr>
			<tr>
				<th style="width: 40%">AGRUPAMIENTO</th>
				<td>{{$recategorizacion->Categoria_inicial->Agrupamiento->desc_agrup}}</td>
			</tr>
		</table>
		<br>
		<table id="customers">
			<tr>
				<th style="width: 40%">CATEGORÍA INICIAL</th>
				<td>{{$recategorizacion->Categoria_inicial->descripcion}}</td>
			</tr>
			<tr>
				<th style="width: 40%">FECHA INICIAL</th>
				<td>{{date('d/m/Y',strtotime($recategorizacion->fecha_inicial))}}</td>
			</tr>
		</table>
		<br>
		<table id="customers">
			<tr>
				<th style="width: 40%">CATEGORÍA ASCENSO</th>
				<td>{{$recategorizacion->Categoria_ascenso->descripcion}}</td>
			</tr>
			<tr>
				<th style="width: 40%">FECHA COMPUTO</th>
				<td>{{date('d/m/Y',strtotime($recategorizacion->fecha_ascenso))}}</td>
			</tr>
		</table>

		<p>Usuario: {{$recategorizacion->User->name}}</p>
	</div>
</body>
</html>