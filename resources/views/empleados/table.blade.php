<div class="table-responsive">
    <table class="table" id="empleados-table">
        <thead>
            <tr>
                <th>Apellido y Nombres</th>
                <th>Nro Dni</th>
                <th>Nro Cuil</th>
                <th>Nro Legajo</th>
                <th colspan="3">Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach($empleados as $empleado)
            <tr>
                <td>{!! $empleado->apynom !!}</td>
                <td>{!! $empleado->nro_dni !!}</td>
                <td>{!! $empleado->nro_cuil !!}</td>
                <td>{!! $empleado->nro_legajo !!}</td>
                <td>
                    {!! Form::open(['route' => ['empleados.destroy', $empleado->id], 'method' => 'delete']) !!}
                        <a href="{!! route('empleados.show', [$empleado->id]) !!}" class='btn btn-info btn-block btn-xs'>Ver</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-block btn-xs btn-danger', 'onclick' => "return confirm('Esta Seguro?')"]) !!}
                        {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
