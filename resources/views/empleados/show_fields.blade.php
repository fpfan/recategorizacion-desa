<div class="col-sm-12" style="padding-top: 20px; padding-right: 20px;">
<table class="table table-bordered table-striped" style="text-transform: uppercase;">
    <tr>
        <th>Nro de Documento</th>
        <td>{{$empleado->nro_dni}}</td>
    </tr>
    <tr>
        <th>Nro de Expediente</th>
        <td>{{$empleado->nro_expediente}}</td>
    </tr>
    <tr>
        <th>Nro de Legajo</th>
        <td>{{$empleado->nro_legajo}}</td>
    </tr>
    @if($empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->id == 2)
        @if($empleadoRecategorizacion->Categoria_inicial->Agrupamiento->id == 3 OR $empleadoRecategorizacion->Categoria_inicial->Agrupamiento->id == 6)
        <tr>
            <th style="width: 40%">TITULO</th>
            <td>{{$empleadoRecategorizacion->Titulo->descripcion}}</td>
            </tr>
        @endif
        <tr>
        <th style="width: 40%">TIENE CAPACITACIÓN</th>
            <td>@if($empleadoRecategorizacion->tiene_capacitacion) Si @else No @endif</td>
        </tr>
    @endif

    @if($empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->id == 3)
    <tr>
        <th style="width: 40%">LOCALIDAD TRABAJA</th>
        <td>{{$empleadoRecategorizacion->Localidad->descripcion}}</td>
    </tr>
    @endif
</table>
</div>

<div class="col-sm-12" style="padding-top: 20px; padding-right: 20px;">
    <table class="table table-bordered table-striped" >
        <tr>
                <th style="width: 40%">RÉGIMEN LABORAL</th>
                <td>{{$empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->descripcion}}</td>
            </tr>
            <tr>
                <th style="width: 40%">AGRUPAMIENTO</th>
                <td>{{$empleadoRecategorizacion->Categoria_inicial->Agrupamiento->desc_agrup}}</td>
            </tr>
        </table>
        <br>
        <table class="table table-bordered table-striped">
            <tr>
                <th style="width: 40%">CATEGORÍA INICIAL</th>
                <td>{{$empleadoRecategorizacion->Categoria_inicial->descripcion}}</td>
            </tr>
            <tr>
                <th style="width: 40%">FECHA INICIAL</th>
                <td>{{date('d/m/Y',strtotime($empleadoRecategorizacion->fecha_inicial))}}</td>
            </tr>
        </table>
        <br>
        <table class="table table-bordered table-striped">
            <tr>
                <th style="width: 40%">CATEGORÍA ASCENSO</th>
                <td>{{$empleadoRecategorizacion->Categoria_ascenso->descripcion}}</td>
            </tr>
            <tr>
                <th style="width: 40%">FECHA COMPUTO</th>
                <td>{{date('d/m/Y',strtotime($empleadoRecategorizacion->fecha_ascenso))}}</td>
            </tr>
</table>
</div>

<div class="col-sm-12">
	<hr>
	<p><b>Usuario:</b> {{$empleadoRecategorizacion->User->name}}</p>
	<hr>
</div>
