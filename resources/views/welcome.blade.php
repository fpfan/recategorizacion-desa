<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Recategorización | Función Pública</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
           <!-- Tell the browser to be responsive to screen width -->
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

            <!-- Bootstrap 3.3.7 -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

            <!-- Font Awesome -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

            <!-- Ionicons -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

            <!-- Theme style -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

            <!-- iCheck -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">
    </head>
    <body>
            @if (Route::has('login'))
                
                    @auth
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center" style="padding-top: 10%">
                            <h2> 
                                Recategorización 
                                <hr>
                                <small>Secretaría de la Función Pública</small>
                            </h2>
                            <br>
                            <img src="{{asset('images/icono.png')}}" height="100px">
                            <a class="btn btn-success" href="{{ url('/home') }}">Inicio</a>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-sm-4"></div>
                       <div class="login-box">
                        <div class="login-logo">
                            <img src="{{asset('images/funcion_publica.png')}}" height="100px">
                            <hr>
                            <a href="#"><b>Recategorización</b></a>
                            <hr>
                        </div>

                        <!-- /.login-logo -->
                        <div class="login-box-body">

                            <form method="post" action="{{ url('/login') }}">
                                {!! csrf_field() !!}

                                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="row text-center">
                                  
                                    <!-- /.col -->
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>

                         </div>
                            <!-- /.login-box-body -->
                    </div>
                    
                    <div class="col-sm-4"></div>
                </div>
                        
            @endauth

        </div>
    @endif
    <footer class="text-center">
        <hr>
        <b>Secretaría de la Función Pública</b> | Río Negro
    </footer>
     
           
    </body>
</html>
