@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Editar Roles  Usuario
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($roleUser, ['route' => ['roleUsers.update', $roleUser->id], 'method' => 'patch']) !!}

                        @include('role__users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection