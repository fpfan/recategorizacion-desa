<!-- User Id Field -->

<div class="form-group col-sm-4">
@if($btn == 'crear')
    {!! Form::label('user_id', 'Usuario:') !!}
    {!! Form::select('user_id', $usuarios, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar','required' => 'true']) !!}
@else
	{!! Form::label('user_id', 'Usuario:') !!}
	<br>{!! $roleUser->usuario->usuario !!}
@endif
</div>

<!-- Role Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('role_id', 'Rol:') !!}
    {!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-4 text-center">
	<br>    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('roleUsers.index') !!}" class="btn btn-default">Cancelar</a>
</div>
