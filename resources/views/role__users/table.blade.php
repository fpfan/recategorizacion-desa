<form action="{!! route('buscar_usuario_rol') !!}" method="GET" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="busqueda" class="form-control" placeholder="Buscar por Rol, Usuario, DNI o Apellido y Nombre"/>
          <span class="input-group-btn">
            <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form>

<div class="table-responsive">
    <table class="table" id="roleUsers-table">
        <thead>
            <tr>
                <th>Usuario</th>
                <th>Rol</th>
                <th colspan="3">Acción</th>
            </tr>
        </thead>
        <tbody>
        @foreach($roleUsers as $roleUser)
            <tr>
                <td>{!! $roleUser->usuario->name !!}</td>
                <td>{!! $roleUser->rol->description !!}</td>
                <td>
                    {!! Form::open(['route' => ['roleUsers.destroy', $roleUser->id], 'method' => 'delete']) !!}
                        <a href="{!! route('roleUsers.show', [$roleUser->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('roleUsers.edit', [$roleUser->id]) !!}" class='btn btn-primary btn-xs'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Está seguro?')"]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{ $roleUsers->links() }}
