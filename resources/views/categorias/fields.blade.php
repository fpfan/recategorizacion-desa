<!-- Cod Reg Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cod_reg', 'Cod Reg:') !!}
    {!! Form::number('cod_reg', null, ['class' => 'form-control']) !!}
</div>

<!-- Cod Agrup Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cod_agrup', 'Cod Agrup:') !!}
    {!! Form::number('cod_agrup', null, ['class' => 'form-control']) !!}
</div>

<!-- Cod Cat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cod_cat', 'Cod Cat:') !!}
    {!! Form::number('cod_cat', null, ['class' => 'form-control']) !!}
</div>

<!-- Inicial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inicial', 'Inicial:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('inicial', 0) !!}
        {!! Form::checkbox('inicial', '1', null) !!}
    </label>
</div>


<!-- Siguiente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('siguiente', 'Siguiente:') !!}
    {!! Form::number('siguiente', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Titulo Minimo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo_minimo', 'Titulo Minimo:') !!}
    {!! Form::number('titulo_minimo', null, ['class' => 'form-control']) !!}
</div>

<!-- Capacitacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('capacitacion', 'Capacitacion:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('capacitacion', 0) !!}
        {!! Form::checkbox('capacitacion', '1', null) !!}
    </label>
</div>


<!-- Anios Promocion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anios_promocion', 'Anios Promocion:') !!}
    {!! Form::number('anios_promocion', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Agrupamiento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_agrupamiento', 'Id Agrupamiento:') !!}
    {!! Form::number('id_agrupamiento', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categorias.index') !!}" class="btn btn-default">Cancel</a>
</div>
