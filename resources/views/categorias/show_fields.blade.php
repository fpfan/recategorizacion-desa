<!-- Cod Reg Field -->
<div class="form-group">
    {!! Form::label('cod_reg', 'Cod Reg:') !!}
    <p>{!! $categoria->cod_reg !!}</p>
</div>

<!-- Cod Agrup Field -->
<div class="form-group">
    {!! Form::label('cod_agrup', 'Cod Agrup:') !!}
    <p>{!! $categoria->cod_agrup !!}</p>
</div>

<!-- Cod Cat Field -->
<div class="form-group">
    {!! Form::label('cod_cat', 'Cod Cat:') !!}
    <p>{!! $categoria->cod_cat !!}</p>
</div>

<!-- Inicial Field -->
<div class="form-group">
    {!! Form::label('inicial', 'Inicial:') !!}
    <p>{!! $categoria->inicial !!}</p>
</div>

<!-- Siguiente Field -->
<div class="form-group">
    {!! Form::label('siguiente', 'Siguiente:') !!}
    <p>{!! $categoria->siguiente !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $categoria->descripcion !!}</p>
</div>

<!-- Titulo Minimo Field -->
<div class="form-group">
    {!! Form::label('titulo_minimo', 'Titulo Minimo:') !!}
    <p>{!! $categoria->titulo_minimo !!}</p>
</div>

<!-- Capacitacion Field -->
<div class="form-group">
    {!! Form::label('capacitacion', 'Capacitacion:') !!}
    <p>{!! $categoria->capacitacion !!}</p>
</div>

<!-- Anios Promocion Field -->
<div class="form-group">
    {!! Form::label('anios_promocion', 'Anios Promocion:') !!}
    <p>{!! $categoria->anios_promocion !!}</p>
</div>

<!-- Id Agrupamiento Field -->
<div class="form-group">
    {!! Form::label('id_agrupamiento', 'Id Agrupamiento:') !!}
    <p>{!! $categoria->id_agrupamiento !!}</p>
</div>

