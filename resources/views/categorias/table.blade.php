<div class="table-responsive">
    <table class="table" id="categorias-table">
        <thead>
            <tr>
                <th>Cod Reg</th>
        <th>Cod Agrup</th>
        <th>Cod Cat</th>
        <th>Inicial</th>
        <th>Siguiente</th>
        <th>Descripcion</th>
        <th>Titulo Minimo</th>
        <th>Capacitacion</th>
        <th>Anios Promocion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categorias as $categoria)
            <tr>
            <td>{!! $categoria->Agrupamiento->Regimen->descripcion !!}</td>
            <td>{!! $categoria->Agrupamiento->desc_agrup !!}</td>
            <td>{!! $categoria->cod_cat !!}</td>
            <td>{!! $categoria->inicial !!}</td>
            <td>{!! $categoria->siguiente !!}</td>
            <td>{!! $categoria->descripcion !!}</td>
            <td>{!! $categoria->titulo_minimo !!}</td>
            <td>{!! $categoria->capacitacion !!}</td>
            <td>{!! $categoria->anios_promocion !!}</td>
                <td>
                    {!! Form::open(['route' => ['categorias.destroy', $categoria->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('categorias.show', [$categoria->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('categorias.edit', [$categoria->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
