@php
 $i = 0
@endphp

@foreach($permisos as $permiso)
<div class="form-group col-sm-2">
    {!! $permiso->name !!}
</div>
@php
 $i = $i + 1
@endphp

@if($i == 4)
<div class="form-group col-sm-4">
        {!! Form::open(['route' => ['permisos.destroy', $permiso->id], 'method' => 'delete']) !!}
            <div class='btn-group'>
                <a href="{!! route('permisos.show', [$permiso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                <a href="{!! route('permisos.edit', [$permiso->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            </div>
        {!! Form::close() !!}
    </div>
@php
 $i = 0
@endphp

@endif
@endforeach
