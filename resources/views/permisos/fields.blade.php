
<!-- Name Field -->
<div class="form-group col-sm-12">
	<h4>Se agregarán los permisos de lectura, creación, edición y eliminación a la tabla: </h4>
</div>
<div class="form-group col-sm-6">
{!! Form::text('tabla', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('permisos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
