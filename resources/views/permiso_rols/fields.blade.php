
@foreach($roles as $rol)
    <div class="form-group col-sm-12">
        {!! Form::hidden('role_id', $rol->id) !!}
        <h4><b>{{$rol->name}}</b></h4>

    <div class="table-responsive">
        <table class="table" id="permisoRols-table">
            <thead>
                <tr>
                    <th>Tabla</th>
                    <th>Ver</th>
                    <th>Crear</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                    <th>Seleccionar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permisos_roles[$rol->name] as $permiso)
                        @if($permiso['tabla'] != $tabla)
                        <tr>
                            <td>{{ $permiso['tabla'] }}</td>
                            @php
                                $tabla = $permiso['tabla'];
                            @endphp
                        @endif

                        @if($permiso['permiso'] == 'ver_'.$tabla)
                            @if($permiso['tiene_permiso'])
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', 'true', ['id' => $permiso['permiso']]) !!}
                                </td>
                            @else
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', '',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @endif
                        @endif
                        
                        @if($permiso['permiso'] == 'crear_'.$tabla)
                            @if($permiso['tiene_permiso'])
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', 'true',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @else
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', '',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @endif
                        @endif

                        @if($permiso['permiso'] == 'editar_'.$tabla)
                            @if($permiso['tiene_permiso'])
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', 'true',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @else
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', '',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @endif
                        @endif

                        @if($permiso['permiso'] == 'eliminar_'.$tabla)
                            @if($permiso['tiene_permiso'])
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', 'true',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @else
                                <td>{!! Form::hidden($permiso['permiso'], 0) !!}
                                    {!! Form::checkbox($permiso['permiso'], '1', '',  ['id' => $permiso['permiso']]) !!}
                                </td>
                            @endif
                            <td>
                           <input type="checkbox" id="all_{{$tabla}}" onClick="seleccionar_todo(this.checked, '{{$permiso['tabla']}}')"></td>
                            </tr>
                        @endif
                @endforeach
            </tbody>
        </table>
    </div>  
</div>        
        
@endforeach

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('permisoRols.index') !!}" class="btn btn-default">Cancelar</a>
</div>


