<!-- Role Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role_id', 'Role Id:') !!}
    {!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'placeholder' => 'seleccione']) !!}
</div>


<div class="form-group col-sm-12">

<div class="form-group col-sm-4">
    Tabla
</div>
<div class="form-group col-sm-2">
    Ver
</div>
<div class="form-group col-sm-2">
    Crear
</div>
<div class="form-group col-sm-2">
    Editar
</div>
<div class="form-group col-sm-2">
    Eliminar
</div>

@foreach($permisos as $permiso)
    @if($permiso->tabla != $tabla)
        <div class="form-group col-sm-4">
            {{$permiso->tabla}}
        </div>
        @php
            $tabla = $permiso->tabla
        @endphp
    @endif

    <div class="form-group col-sm-2">
            {!! Form::hidden('ver'.'_'.$permiso->tabla, 0) !!}
            {!! Form::checkbox('ver'.'_'.$permiso->tabla, '1', null) !!}
    </div>

    <div class="form-group col-sm-2">
        {!! Form::hidden('crear'.'_'.$permiso->tabla, 0) !!}
        {!! Form::checkbox('crear'.'_'.$permiso->tabla, '1', null) !!}
    </div>
    
    <div class="form-group col-sm-2">
            {!! Form::hidden('editar'.'_'.$permiso->tabla, 0) !!}
            {!! Form::checkbox('editar'.'_'.$permiso->tabla, '1', null) !!}
    </div>

    <div class="form-group col-sm-2">
        {!! Form::hidden('eliminar'.'_'.$permiso->tabla, 0) !!}
        {!! Form::checkbox('eliminar'.'_'.$permiso->tabla, '1', null) !!}
    </div>
@endforeach

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('permisoRols.index') !!}" class="btn btn-default">Cancelar</a>
</div>
