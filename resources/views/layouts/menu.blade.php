<!--
<li class="{{ Request::is('empleados*') ? 'active' : '' }}">
    <a href="{!! route('empleados.index') !!}"><i class="fa fa-edit"></i><span>Empleados</span></a>
</li>-->
<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{!! route('home') !!}"  style="color: white"><i class="fa fa-home"></i><span> Inicio</span></a>
</li>

<li class="{{ Request::is('empleadoRecategorizacions*') ? 'active' : '' }}">    
    <a href="{!! route('empleadoRecategorizacions.index') !!}" style="color: white"><i class="fa fa-edit"></i><span> Recategorizacion</span></a>
</li>

<hr>
@if(auth()->user()->role->name == 'administrador')
<div class="ac">
    
    <input class="ac-input" id="ac-tablas-aux" name="ac-1" type="checkbox" />
    <label class="ac-label" for="ac-tablas-aux" style="color:white"><i class="fa fa-angle-down"></i>Tablas Auxiliares</label>

    <article class="ac-text">

    <li class="{{ Request::is('regimens*') ? 'active' : '' }}">
        <a href="{!! route('regimens.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Regimens</span></a>
    </li>

    <li class="{{ Request::is('agrupamientos*') ? 'active' : '' }}">
        <a href="{!! route('agrupamientos.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Agrupamientos</span></a>
    </li>

    <li class="{{ Request::is('categorias*') ? 'active' : '' }}">
        <a href="{!! route('categorias.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Categorias</span></a>
    </li>

    <li class="{{ Request::is('provincias*') ? 'active' : '' }}">
        <a href="{!! route('provincias.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Provincias</span></a>
    </li>

    <li class="{{ Request::is('localidades*') ? 'active' : '' }}">
        <a href="{!! route('localidades.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Localidades</span></a>
    </li>

    <li class="{{ Request::is('zonas*') ? 'active' : '' }}">
        <a href="{!! route('zonas.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Zonas</span></a>
    </li>

    <li class="{{ Request::is('zonaLocalidads*') ? 'active' : '' }}">
        <a href="{!! route('zonaLocalidads.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Zona Localidads</span></a>
    </li>
    <li class="{{ Request::is('titulos*') ? 'active' : '' }}">
        <a href="{!! route('titulos.index') !!}" class="ac-label"><i class="fa fa-edit"></i><span>Titulos</span></a>
    </li>

    </article>
</div>



<hr>
<div class="ac">
    <input class="ac-input" id="ac-usuarios" name="ac-1" type="checkbox" />
    <label class="ac-label" for="ac-usuarios" style="color:white"><i class="fa fa-angle-down"></i> Gestión de Usuarios</label>
    
    <article class="ac-text">
        <li class="{{ Request::is('usuarios*') ? 'active' : '' }}">
            <a href="{!! route('usuarios.index') !!}"  class="ac-label"><i class="fa fa-user a-menu"></i><span class="a-menu"> Usuarios</span></a>
        </li>

        <li class="{{ Request::is('rols*') ? 'active' : '' }}">
            <a href="{!! route('rols.index') !!}"  class="ac-label"><i class="fa fa-user a-menu"></i><span class="a-menu"> Roles</span></a>
        </li>

        <li class="{{ Request::is('permisos*') ? 'active' : '' }}">
            <a href="{!! route('permisos.index') !!}" class="ac-label"><i class="fa fa-user a-menu"></i><span class="a-menu"> Permisos</span></a>
        </li>

        <li class="{{ Request::is('permisoRols*') ? 'active' : '' }}">
            <a href="{!! route('permisoRols.index') !!}" class="ac-label"><i class="fa fa-user a-menu"></i><span class="a-menu"> Permisos Rol</span></a>
        </li>
    </article>
</div>

@endif