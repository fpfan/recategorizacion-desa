@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Zona Localidad
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($zonaLocalidad, ['route' => ['zonaLocalidads.update', $zonaLocalidad->id], 'method' => 'patch']) !!}

                        @include('zona_localidads.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection