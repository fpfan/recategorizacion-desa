<!-- Id Zona Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_zona', 'Id Zona:') !!}
    {!! Form::number('id_zona', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Localidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_localidad', 'Id Localidad:') !!}
    {!! Form::number('id_localidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('zonaLocalidads.index') !!}" class="btn btn-default">Cancel</a>
</div>
