<div class="table-responsive">
    <table class="table" id="zonaLocalidads-table">
        <thead>
            <tr>
                <th>Id Zona</th>
        <th>Id Localidad</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($zonaLocalidads as $zonaLocalidad)
            <tr>
                <td>{!! $zonaLocalidad->id_zona !!}</td>
            <td>{!! $zonaLocalidad->id_localidad !!}</td>
                <td>
                    {!! Form::open(['route' => ['zonaLocalidads.destroy', $zonaLocalidad->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('zonaLocalidads.show', [$zonaLocalidad->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('zonaLocalidads.edit', [$zonaLocalidad->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
