@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cálculo Recategorización
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @if($empleadoRecategorizacion['btn'] == 'crear')
                    {!! Form::open(['route' => 'empleadoRecategorizacions.store']) !!}
                    @else
                    {!! Form::model($empleadoRecategorizacion, ['route' => ['empleadoRecategorizacions.update', $empleadoRecategorizacion['id']], 'method' => 'patch']) !!}
                    @endif
                    <!-- Id Categoria Inicial Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('id_categoria_inicial', 'Categoria Inicial:') !!}
                        {!! Form::hidden('id_categoria_inicial', $empleadoRecategorizacion['categoria_inicial']->id, ['class' => 'form-control']) !!}
                        <input type="text" name="categoria_inicial" class="form-control" value="{{$empleadoRecategorizacion['categoria_inicial']->descripcion}}" readonly="">
                    </div>

                    <!-- Id Categoria Ascenso Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('id_categoria_ascenso', 'Categoria Ascenso:') !!}
                        {!! Form::hidden('id_categoria_ascenso', $empleadoRecategorizacion['categoria_ascenso']->id, ['class' => 'form-control', 'readonly' => '']) !!}
                        <input type="text" name="categoria_ascenso" class="form-control" value="{{$empleadoRecategorizacion['categoria_ascenso']->descripcion}}" readonly="">
                    </div>

                    <!-- Fecha Inicial Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('fecha_inicial', 'Fecha Inicial:') !!}
                        {!! Form::hidden('fecha_inicial', $empleadoRecategorizacion['fecha_inicial']) !!}
                        {!! Form::text('fecha_inicial_1', date("d/m/Y", strtotime($empleadoRecategorizacion['fecha_inicial'])), ['class' => 'form-control', 'readonly' => '']) !!}</p>
                    </div>

                    <!-- Fecha Ascenso Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('fecha_ascenso', 'Fecha Computo:') !!}
                        {!! Form::hidden('fecha_ascenso', $empleadoRecategorizacion['fecha_ascenso']) !!}
                        {!! Form::text('fecha_ascenso_1', date("d/m/Y", strtotime($empleadoRecategorizacion['fecha_ascenso'])), ['class' => 'form-control', 'readonly' => '']) !!}
                        </p>
                    </div>

                                        <!-- Fecha Inicial Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('fecha_final', 'Fecha final:') !!}
                        {!! Form::hidden('fecha_final', $empleadoRecategorizacion['fecha_final']) !!}
                        {!! Form::text('fecha_final_1', date("d/m/Y", strtotime($empleadoRecategorizacion['fecha_final'])), ['class' => 'form-control', 'readonly' => '']) !!}</p>
                    </div>

                    @if($empleadoRecategorizacion['localidad_trabaja'] != null)
                    <div class="form-group col-sm-6">
                        {!! Form::label('id_localidad_trabaja', 'Id Localidad Trabaja:') !!}
                        {!! Form::hidden('id_localidad_trabaja', $empleadoRecategorizacion['localidad_trabaja']->id, ['class' => 'form-control', 'readonly' => '']) !!}
                        <input type="text" name="localidad_trabaja" class="form-control" value="{{$empleadoRecategorizacion['localidad_trabaja']->descripcion}}" readonly="">
                    </div>
                    @else
                        @if(($empleadoRecategorizacion['categoria_inicial']->Agrupamiento->id == 3 OR $empleadoRecategorizacion['categoria_inicial']->Agrupamiento->id == 6) AND $empleadoRecategorizacion['categoria_inicial']->Agrupamiento->cod_reg == 2)
                        <!-- Id Localidad Trabaja Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('id_titulo', 'Título:') !!}
                            {!! Form::hidden('id_titulo', $empleadoRecategorizacion['titulo']->id, ['class' => 'form-control', 'readonly' => '']) !!}
                            <input type="text" name="titulo" class="form-control" value="{{$empleadoRecategorizacion['titulo']->descripcion}}" readonly="">
                        </div>
                        @endif
                        <!-- Tiene Capacitacion Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('tiene_capacitacion', 'Tiene Capacitacion:') !!}
                            {!! Form::hidden('tiene_capacitacion', $empleadoRecategorizacion['tiene_capacitacion'], ['class' => 'form-control', 'readonly' => '']) !!}
                            @if($empleadoRecategorizacion['tiene_capacitacion'] == 1)
                            <input type="text" name="" class="form-control" value="Si" readonly="">
                            @else
                            <input type="text" name="" class="form-control" value="No" readonly="">
                            @endif
                        </div>
                    @endif

                    <div class="form-group col-sm-12 text-center">
                        <hr>
                        <a href="{!! route('empleadoRecategorizacions.index') !!}" class="btn btn-default">Volver</a>
                            <button class="btn btn-primary" type="submit">Guardar Cálculo</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection