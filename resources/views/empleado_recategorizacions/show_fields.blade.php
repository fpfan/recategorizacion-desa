<!-- Id Empleado Field -->
<div class="form-group">
    {!! Form::label('id_empleado', 'Id Empleado:') !!}
    <p>{!! $empleadoRecategorizacion->id_empleado !!}</p>
</div>

<!-- Id Categoria Inicial Field -->
<div class="form-group">
    {!! Form::label('id_categoria_inicial', 'Id Categoria Inicial:') !!}
    <p>{!! $empleadoRecategorizacion->id_categoria_inicial !!}</p>
</div>

<!-- Id Categoria Ascenso Field -->
<div class="form-group">
    {!! Form::label('id_categoria_ascenso', 'Id Categoria Ascenso:') !!}
    <p>{!! $empleadoRecategorizacion->id_categoria_ascenso !!}</p>
</div>

<!-- Fecha Inicial Field -->
<div class="form-group">
    {!! Form::label('fecha_inicial', 'Fecha Inicial:') !!}
    <p>{!! $empleadoRecategorizacion->fecha_inicial !!}</p>
</div>

<!-- Fecha Ascenso Field -->
<div class="form-group">
    {!! Form::label('fecha_ascenso', 'Fecha Computo:') !!}
    <p>{!! $empleadoRecategorizacion->fecha_ascenso !!}</p>
</div>

<!-- Tiene Capacitacion Field -->
<div class="form-group">
    {!! Form::label('tiene_capacitacion', 'Tiene Capacitacion:') !!}
    <p>{!! $empleadoRecategorizacion->tiene_capacitacion !!}</p>
</div>

<!-- Id Localidad Trabaja Field -->
<div class="form-group">
    {!! Form::label('id_localidad_trabaja', 'Id Localidad Trabaja:') !!}
    <p>{!! $empleadoRecategorizacion->id_localidad_trabaja !!}</p>
</div>

<!-- Id User Field -->
<div class="form-group">
    {!! Form::label('id_user', 'Id User:') !!}
    <p>{!! $empleadoRecategorizacion->id_user !!}</p>
</div>

