<div class="table-responsive">
    <table class="table table-bordered table-hover" id="empleadoRecategorizacions-table">
        <thead>
            <tr bgcolor="lightblue">
                <th>Apellido y Nombre</th>
                <th>DNI</th>
                <th>Expediente</th> 
                <th>Legajo</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! $empleadoRecategorizacion->Empleado->apynom !!}</td>
                <td>{!! $empleadoRecategorizacion->Empleado->nro_dni !!}</td>
                <td>{!! $empleadoRecategorizacion->Empleado->nro_expediente !!}</td>
                <td>{!! $empleadoRecategorizacion->Empleado->nro_legajo !!}</td>
                <td><a href="{!! route('empleados.edit', [$empleadoRecategorizacion->id_empleado]) !!}" class='btn btn-primary btn-block btn-xs'>Editar</a></td>
            </tr>
        </tbody>
    </table>
</div>


<div class="row">
    <div class="col-sm-4">
        <label>Régimen</label>
            <select class="form-control" name="id_regimen" id="id_regimen" onChange="getAgrupamientos('{{request()->root()}}');">
                <option value="{{$empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->id}}">{{$empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->descripcion}}</option>
                    @foreach($regimenes as $regimen)
                        @if($regimen->id != $empleadoRecategorizacion->Categoria_inicial->cod_reg)
                            <option value="{!! $regimen->id !!}">{!! $regimen->descripcion !!}
                            </option>
                        @endif
                    @endforeach
            </select>
    </div>

    @if($empleadoRecategorizacion->Categoria_inicial != null)
        <div class="col-sm-4" id="agrupamiento_ajax">
            <label>Agrupamiento</label>
            
            <select class="form-control" name="id_agrupamiento" id="id_agrupamiento" onChange="getCategorias('{{request()->root()}}');">
                <option value="{{$empleadoRecategorizacion->Categoria_inicial->Agrupamiento->id}}">{{$empleadoRecategorizacion->Categoria_inicial->Agrupamiento->desc_agrup}}</option>
                @foreach ($agrupamientos as $agrup)
                    @if($agrup->id != $empleadoRecategorizacion->Categoria_inicial->Agrupamiento->id)
                        <option value="{{ $agrup->id }}">
                            {{ $agrup->desc_agrup }}            
                        </option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="col-sm-4" id="categoria_ajax">
            <label>Categorias</label>
                <select class="form-control" id="id_categoria_inicial" name="id_categoria_inicial" required>
                    <option value="{{$empleadoRecategorizacion->Categoria_inicial->id}}">{{$empleadoRecategorizacion->Categoria_inicial->descripcion}}</option>
                    @foreach ($categorias as $categoria)
                        @if($categoria->id != $empleadoRecategorizacion->Categoria_inicial->id)
                        <option value="{!! $categoria->id !!}">{!! $categoria->descripcion !!}</option>
                    @endif
                    @endforeach
                </select>
        </div>
    @else
        <div class="col-sm-4" id="agrupamiento_ajax">
            <label>Agrupamiento</label>
                <select class="form-control" disabled="">
                    <option value="">Seleccione</option>
                </select>
        </div>
         
        <div class="col-sm-4" id="categoria_ajax">
            <label>Categoría</label>
                <select class="form-control" disabled="">
                    <option value="">Seleccione</option>
                </select>
        </div>
    @endif

    <div class="col-sm-12"><br></div>
    @if($empleadoRecategorizacion->fecha_inicial)
        <div class="form-group col-sm-4">
            {!! Form::label('fecha_inicial', 'Fecha Inicial:') !!}
            {!! Form::date('fecha_inicial', $empleadoRecategorizacion->fecha_inicial, ['class' => 'form-control','id'=>'fecha_inicial', 'required' => 'true', 'placeholder' => 'AAAA-MM-DD']) !!}
        </div>       

    @endif
   <div class="form-group col-sm-4">
            {!! Form::label('fecha_final', 'Fecha Final:') !!}
            {!! Form::date('fecha_final', $empleadoRecategorizacion->fecha_final, ['class' => 'form-control','id'=>'fecha_final', 'required' => 'true', 'placeholder' => 'AAAA-MM-DD']) !!}
        </div>

    @if($empleadoRecategorizacion->Categoria_inicial->cod_reg == 3)
        <div class="col-sm-4" id="localidad" style="display: block;">
            <label>Localidad Trabaja</label>
            <select class="form-control" id="id_localidad_trabaja" name="id_localidad_trabaja">
                <option value="{{$empleadoRecategorizacion->id_localidad_trabaja}}">{{$empleadoRecategorizacion->Localidad->descripcion}}</option>
                @foreach($localidades as $localidad)
                    @if($localidad->id != $empleadoRecategorizacion->id_localidad_trabaja)
                        <option value="{{$localidad->id}}">{{$localidad->descripcion}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    @else
        <div class="col-sm-4" id="localidad" style="display: none;">
            <label>Localidad Trabaja</label>
            <select class="form-control" id="id_localidad_trabaja" name="id_localidad_trabaja">
                <option value="">Seleccione</option>
                    @foreach($localidades as $localidad)
                        <option value="{{$localidad->id}}">{{$localidad->descripcion}}</option>
                    @endforeach
            </select>
        </div>
    @endif

    @if($empleadoRecategorizacion->Categoria_inicial->id_agrupamiento == 6 || $empleadoRecategorizacion->Categoria_inicial->id_agrupamiento == 3)
        <div class="col-sm-4" id="titulo" style="display: block;">
            <label>Título</label>
                <select class="form-control" id="id_titulo" name="id_titulo">
                    <option value="{{$empleadoRecategorizacion->id_titulo}}">{{$empleadoRecategorizacion->Titulo->descripcion}}</option>
                    @foreach($titulos as $titulo)
                        <option value="{{$titulo->id}}">{{$titulo->descripcion}}</option>
                    @endforeach
                </select>
        </div>
    @else
        <div class="col-sm-4" id="titulo" style="display: none;">
            <label>Título</label>
            <select class="form-control" id="id_titulo" name="id_titulo">
                <option value="">Seleccione</option>
                    @foreach($titulos as $titulo)
                        <option value="{{$titulo->id}}">{{$titulo->descripcion}}</option>
                    @endforeach
            </select>
        </div>
    @endif
    <div class="col-sm-12"></div>
    @if($empleadoRecategorizacion->Categoria_inicial->cod_reg == 2)
        <div class="col-sm-2" id="tiene_cap" style="display: block;">
            {!! Form::label('tiene_capacitacion', '¿Tiene capacitación?: ') !!}
            <br>
            <label class="checkbox-inline">
                {!! Form::radio('tiene_capacitacion', '1', $empleadoRecategorizacion->tiene_capacitacion,['required' => '']) !!} Si
                {!! Form::radio('tiene_capacitacion', '0', $empleadoRecategorizacion->tiene_capacitacion,['required' => '']) !!} No
            </label>
        </div>
    @else
        <div class="col-sm-2" id="tiene_cap" style="display: none;">
            {!! Form::label('tiene_capacitacion', '¿Tiene capacitación?: ') !!}
            <br>
            <label class="checkbox-inline">
                {!! Form::radio('tiene_capacitacion', '1', $empleadoRecategorizacion->tiene_capacitacion,['required' => '']) !!} Si
                {!! Form::radio('tiene_capacitacion', '0', $empleadoRecategorizacion->tiene_capacitacion,['required' => '']) !!} No
            </label>
        </div>
    @endif


        <div class="col-sm-12 text-center">
            <hr>
            <button type="submit" class="btn btn-success">Calcular</button>
            <a href="{!! route('empleadoRecategorizacions.edit', $empleadoRecategorizacion->id) !!}" class="btn btn-default">Reestablecer Campos</a>
        </div>

        </div>
        <div class="col-sm-3"></div>

    </div>