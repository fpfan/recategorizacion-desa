   <label>Categorias</label>
	<select class="form-control" id="id_categoria_inicial" name="id_categoria_inicial" required>
    	<option value="">Seleccione</option>
    	@foreach ($categorias as $categoria)
            <option value="{!! $categoria->id !!}">{!! $categoria->descripcion !!}</option>
        @endforeach
	</select>