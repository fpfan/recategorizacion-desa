@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Calcular Recategorización
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="padding-left: 20px; padding-right: 20px">
                    {!! Form::open(['route' => 'empleadoRecategorizacions.calcular']) !!}
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="btn" id="btn" value="crear">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Régimen</label>
                               <select class="form-control" name="id_regimen" id="id_regimen" onChange="getAgrupamientos('{{request()->root()}}');" required="">
                                    <option value="">Seleccionar</option>
                                    @foreach($regimenes as $regimen)
                                        <option value="{!! $regimen->id !!}">{!! $regimen->descripcion !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4" id="agrupamiento_ajax">
                                <label>Agrupamiento</label>
                                <select class="form-control" disabled="">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                            <div class="col-sm-4" id="categoria_ajax">
                                <label>Categoría</label>
                                <select class="form-control" disabled="">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                            <div class="col-sm-12"><br></div>
                            <div class="col-sm-2">                                
                                <label>Fecha Inicial</label>
                                <br>
                                <input class="form-control" type="date" name="fecha_inicial" id="fecha_inicial" required="">
                            </div>
                            <div class="col-sm-2">                                
                                <label>Fecha Final</label>
                                <br>
                                <input class="form-control" type="date" name="fecha_final" id="fecha_final" required="">
                            </div>

                            <div class="col-sm-4" id="localidad" style="display: none;">
                                <label>Localidad Trabaja</label>
                                <select class="form-control" id="id_localidad_trabaja" name="id_localidad_trabaja">
                                    <option value="">Seleccione</option>
                                    @foreach($localidades as $localidad)
                                        <option value="{{$localidad->id}}">{{$localidad->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4" id="titulo" style="display: none;">
                                <label>Título</label>
                                <select class="form-control" id="id_titulo" name="id_titulo">
                                    <option value="">Seleccione</option>
                                    @foreach($titulos as $titulo)
                                        <option value="{{$titulo->id}}">{{$titulo->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-2" id="tiene_cap" style="display: none;">                                
                                <label>Tiene Capacitación</label>
                                <br>
                                <input type="radio" name="tiene_capacitacion" id="tiene_capacitacion" value="1"> Si
                                <input type="radio" name="tiene_capacitacion" id="tiene_capacitacion" value="0"> No
                            </div>

                            <div class="col-sm-12 text-center">
                                <hr>
                                <button type="submit" class="btn btn-success">
                                    Calcular</button>
                                <button type="reset" class="btn btn-default">Limpiar Campos</button>                               
                            </div>

                            </div>
                            <div class="col-sm-3"></div>

                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

