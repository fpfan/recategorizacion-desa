<form action="{!! route('empleadoRecategorizacions.index') !!}" method="GET" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="busqueda" class="form-control" placeholder="Buscar por Expediente, documento o nombre y apellido"/ style="background-color: white">
          <span class="input-group-btn">
            <button type='submit' id='search-btn' class="btn btn-flat" style="background-color: white"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form>

<div class="table-responsive">
    <table class="table" id="empleadoRecategorizacions-table">
        <thead>
            <tr>
                <th>Expediente</th>
                <th>DNI</th>
                <th>Empleado</th>
                <th>Regimen</th>
                <th>Agrupamiento</th>
                <th>Categoria Inicial</th>
                <th>Categoria Ascenso</th>
                <th>Fecha Inicial</th>
                <th>Fecha Final</th>
                <th>Fecha Computo</th>
                <th colspan="3">Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach($empleadoRecategorizacions as $empleadoRecategorizacion)
            <tr>
                <td>{!! $empleadoRecategorizacion->Empleado->nro_expediente !!}</td>
                <td>{!! $empleadoRecategorizacion->Empleado->nro_dni !!}</td>
                <td>{!! $empleadoRecategorizacion->Empleado->apynom !!}</td>
                <td>{!! $empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->descripcion !!}</td>
                <td>{!! $empleadoRecategorizacion->Categoria_inicial->Agrupamiento->desc_agrup !!}</td>
                <td>{!! $empleadoRecategorizacion->Categoria_inicial->descripcion !!}</td>
                <td>{!! $empleadoRecategorizacion->Categoria_ascenso->descripcion !!}</td>
                <td>{!! date('d/m/Y',strtotime($empleadoRecategorizacion->fecha_inicial)) !!}</td>
                <td>{!! date('d/m/Y',strtotime($empleadoRecategorizacion->fecha_final)) !!}</td>
                <td>{!! date('d/m/Y',strtotime($empleadoRecategorizacion->fecha_ascenso)) !!}</td>
                <td>
                    {!! Form::open(['route' => ['empleados.destroy', $empleadoRecategorizacion->id_empleado], 'method' => 'delete']) !!}
                        <a href="{!! route('empleados.show', [$empleadoRecategorizacion->id_empleado]) !!}" class='btn btn-info btn-block btn-xs'>Ver</a>
                        <a href="{!! route('empleadoRecategorizacions.edit', [$empleadoRecategorizacion->id]) !!}" class='btn btn-primary btn-block btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-block btn-xs btn-danger', 'onclick' => "return confirm('Esta Seguro?')"]) !!}
                        {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
