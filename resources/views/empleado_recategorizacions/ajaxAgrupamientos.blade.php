    <label>Agrupamiento</label>
    <select class="form-control" id="id_agrupamiento" name="id_agrupamiento"  onChange="getCategorias('{{request()->root()}}');" required>
    	<option value="">Seleccione</option>
    	@foreach ($agrupamientos as $agrup)
            <option value="{!! $agrup->id !!}">
            	{!! $agrup->desc_agrup !!}            
            </option>
        @endforeach
    </select>