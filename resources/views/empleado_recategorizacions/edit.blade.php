@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Empleado Recategorizacion
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="padding-left: 20px; padding-right: 20px">
                   {!! Form::model($empleadoRecategorizacion, ['route' => ['empleadoRecategorizacions.calcular'], 'method' => 'post']) !!}
                   <input type="hidden" name="id" id="id" value="{{$empleadoRecategorizacion->id}}">
                   <input type="hidden" name="btn" id="btn" value="editar">

                        @include('empleado_recategorizacions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection