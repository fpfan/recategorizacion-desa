<div class="table-responsive">
    <table class="table" id="titulos-table">
        <thead>
            <tr>
                <th>Nivel</th>
        <th>Descripcion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($titulos as $titulo)
            <tr>
                <td>{!! $titulo->nivel !!}</td>
            <td>{!! $titulo->descripcion !!}</td>
                <td>
                    {!! Form::open(['route' => ['titulos.destroy', $titulo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('titulos.show', [$titulo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('titulos.edit', [$titulo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
