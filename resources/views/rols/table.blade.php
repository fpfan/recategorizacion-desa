<div class="table-responsive">
    <table class="table" id="rols-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th colspan="3"></th>
            </tr>
        </thead>
        <tbody>
        @foreach($rols as $rol)
            <tr>
                <td>{!! $rol->name !!}</td>
            <td>{!! $rol->description !!}</td>
                <td>
                    {!! Form::open(['route' => ['rols.destroy', $rol->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('rols.edit', [$rol->id]) !!}" class='btn btn-primary btn-xs'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Está seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
