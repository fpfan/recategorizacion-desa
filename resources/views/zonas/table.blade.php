<div class="table-responsive">
    <table class="table" id="zonas-table">
        <thead>
            <tr>
                <th>Codigo</th>
        <th>Descripcion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($zonas as $zonas)
            <tr>
                <td>{!! $zonas->codigo !!}</td>
            <td>{!! $zonas->descripcion !!}</td>
                <td>
                    {!! Form::open(['route' => ['zonas.destroy', $zonas->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('zonas.show', [$zonas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('zonas.edit', [$zonas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
