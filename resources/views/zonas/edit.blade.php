@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Zonas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($zonas, ['route' => ['zonas.update', $zonas->id], 'method' => 'patch']) !!}

                        @include('zonas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection