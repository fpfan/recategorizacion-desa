<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Provincia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_provincia', 'Id Provincia:') !!}
    {!! Form::number('id_provincia', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-7">
    {!! Form::label('inhospita', 'Inhospito: ') !!}
    <br>
    <label class="checkbox-inline">
        {!! Form::radio('inhospita', '1', null,['required' => '']) !!} Si
        {!! Form::radio('inhospita', '0', null,['required' => '']) !!} No
	</label>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('localidades.index') !!}" class="btn btn-default">Cancel</a>
</div>
