<form action="{!! route('localidades.index') !!}" method="GET" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="busqueda" class="form-control" style="background-color: white" placeholder="Buscar"/>
          <span class="input-group-btn">
            <button type='submit' id='search-btn' class="btn btn-flat" style="background-color: white"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form>

<div class="table-responsive">
    <table class="table" id="localidades-table">
        <thead>
            <tr>
                <th>Descripcion</th>
                <th>Provincia</th>
                <th>Inhospita</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($localidades as $localidad)
            <tr>
                <td>{!! $localidad->descripcion !!}</td>
                <td>{!! $localidad->Provincia->descripcion !!}</td>
                @if($localidad->inhospita == 0)
                    <td>No</td>
                @else
                    <td>Si</td>
                @endif
                <td>
                    {!! Form::open(['route' => ['localidades.destroy', $localidad->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('localidades.show', [$localidad->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('localidades.edit', [$localidad->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{!! $localidades->appends(['busqueda'=>$busqueda])->links() !!}
