@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Regimen
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($regimen, ['route' => ['regimens.update', $regimen->id], 'method' => 'patch']) !!}

                        @include('regimens.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection