<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUsuariosRequest;
use App\Http\Requests\UpdateUsuariosRequest;
use App\Repositories\UsuariosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;

use App\User;
use App\Role;
use App\Models\Role_User;

class UsuariosController extends AppBaseController
{
    /** @var  UsuariosRepository */
    private $usuariosRepository;

    public function __construct(UsuariosRepository $usuariosRepo)
    {
        $this->usuariosRepository = $usuariosRepo;
    }

    /**
     * Display a listing of the Usuarios.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $usuarios = User::orderBy('updated_at', 'desc')->paginate(20);

        return view('usuarios.index')
            ->with('usuarios', $usuarios)
            ->with('busqueda', $request->busqueda);
    }

    /**
     * Show the form for creating a new Usuarios.
     *
     * @return Response
     */
    public function create()
    {   
        $usuarios = null;
        $roles = Role::orderby('name', 'asc')->pluck('name', 'id');     
        return view('usuarios.create')
                    ->with('btn', 'crear')
                    ->with('usuarios', $usuarios)
                    ->with('roles', $roles);
    }

    /**
     * Store a newly created Usuarios in storage.
     *
     * @param CreateUsuariosRequest $request
     *
     * @return Response
     */
     public function store(CreateUsuariosRequest $request)
    {

        $input = $request->all();

        $password=$input['nro_dni'];
        $input['password'] = bcrypt($password);
        $input['name'] = $input['nombre'].' '.$input['apellido'];
        $nuevousuario = $input['nombre'][0].$input['apellido'];
        
        $user = User::where('usuario','like',$nuevousuario.'%')->get();
        $cantidad= count($user);
        if($cantidad > 0){
            $cantidad++;
            $input['usuario'] = strtolower($nuevousuario.$cantidad);
        }
        else{
            $input['usuario'] = strtolower($nuevousuario);
        }
        $usuarios = $this->usuariosRepository->create($input);

        Flash::success('El usuario '. $input['usuario'] .' se ha agregado correctamente.');

        return redirect(route('usuarios.index'));
    }

    /**
     * Display the specified Usuarios.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $usuario = $this->usuariosRepository->find($id);

        if (empty($usuario)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('usuarios.index'));
        }

        $usuarios = Usuario::where('id_user', $usuario->id)->get();
        return view('usuarios.show')
                ->with('usuario', $usuario);
    }

    /**
     * Show the form for editing the specified Usuarios.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $usuarios = $this->usuariosRepository->find($id);

        if (empty($usuarios)) {
            Flash::error('Usuarios not found');

            return redirect(route('usuarios.index'));
        }
        $roles = Role::orderby('name', 'asc')->pluck('name', 'id');     
        return view('usuarios.edit')->with('usuarios', $usuarios)
                    ->with('btn', 'editar')
                    ->with('roles', $roles);

    }

    /**
     * Update the specified Usuarios in storage.
     *
     * @param int $id
     * @param UpdateUsuariosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsuariosRequest $request)
    {
        $user = $this->usuariosRepository->find($id);
        if($request->usuario != $user->usuario){
            $validated = $request->validate([
                    'usuario' => 'required|unique:users',
            ]);
        }
        if (empty($user)) {
            Flash::error('Usuarios not found');

            return redirect(route('usuarios.index'));
        }
        
        $input=$request->all();

        $usuarios = $this->usuariosRepository->update($input, $id);

        Flash::success('Usuarios actualizado correctamente.');

        return redirect(route('usuarios.index'));
    }

    /**
     * Remove the specified Usuarios from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $usuarios = $this->usuariosRepository->find($id);

        if (empty($usuarios)) {
            Flash::error('Usuarios not found');

            return redirect(route('usuarios.index'));
        }

        $this->usuariosRepository->delete($id);
        
        Flash::success('Usuarios eliminado correctamente.');

        return redirect(route('usuarios.index'));
    }

    public function cambiar_contrasena(){        
        return view('usuarios.cambiar_contrasena');
    }

    public function cambiar_pass(Request $request){
        //dd($request->password_viejo);
        $id = auth()->user()->id;
        $user = User::where('id', $id)->first();
        $password = $user->password;

        
        if (password_verify($request->password_viejo, $password)) {
           if(strlen($request->password_nuevo)>= 8) {
               if($request->password_nuevo == $request->password_viejo){
                    Flash::error('La contraseña nueva no puede ser igual a la contraseña actual');
                    return view('usuarios.cambiar_contrasena');
               }
               else if($request->password_nuevo == $request->password_repetido){
                    $password = bcrypt($request->password_nuevo);
                    User::where('id', $id)->update(array('password'=>$password, 'activo'=>1));
                    Flash::success('El cambio de contraseña fue existoso');
                    return redirect(route('home'));
                }
                else {
                    Flash::error('La nueva contraseña no coincide');
                    return view('usuarios.cambiar_contrasena');
                }
            }
           else {
            Flash::error('La nueva contraseña debe tener al menos 8 caracteres.');
            return view('usuarios.cambiar_contrasena');
            }
        }
        else {
            Flash::error('La contraseña actual ingresada no coincide.');
            return view('usuarios.cambiar_contrasena');
        }

    }

    public function resetear_contrasena($id){
        $user = User::where('id', $id)->first();
        $password = bcrypt($user->nro_dni);
        User::where('id', $id)->update(array('password'=>$password, 'activo'=>0));
        Flash::success('La contraseña ha sido reseteada correctamente.');
        
        $usuarios = User::orderBy('name', 'asc')->paginate(20);
        return redirect(route('usuarios.index'));
    }

    public function buscar_usuario(Request $request){
        $usuarios = User::select('users.*')
                        ->where('name', 'like', '%'.$request->busqueda.'%')
                        ->orWhere('nro_dni', 'like', '%'.$request->busqueda.'%')
                        ->orWhere('usuario', 'like', '%'.$request->busqueda.'%') 
                        ->orWhere('email', 'like', '%'.$request->busqueda.'%') 
                        ->orderBy('name', 'asc')
                        ->paginate(20);
       
        return view('usuarios.index')
                ->with('usuarios', $usuarios)
                ->with('busqueda', $request->busqueda);
    }

    public function buscar_usuario_resetear(Request $request){
        $usuarios = User::where('name', 'like', '%'.$request->busqueda.'%')
                        ->orWhere('nro_dni', 'like', '%'.$request->busqueda.'%')
                        ->orWhere('usuario', 'like', '%'.$request->busqueda.'%') 
                        ->orderBy('name', 'asc')
                        ->paginate(20);
       
        return view('usuarios.resetear')
                ->with('usuarios', $usuarios)
                ->with('busqueda', $request->busqueda);;
    }

}
