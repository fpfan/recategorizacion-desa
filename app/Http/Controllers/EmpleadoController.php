<?php

namespace App\Http\Controllers;



 
use App\Http\Requests\CreateEmpleadoRequest;
use App\Http\Requests\UpdateEmpleadoRequest;
use App\Repositories\EmpleadoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use PDF;
use Dompdf\Adapter\CPDF;
use Dompdf\Dompdf;
use Dompdf\Exception;

use App\Models\Empleado;
use App\Models\EmpleadoRecategorizacion;

class EmpleadoController extends AppBaseController
{
    /** @var  EmpleadoRepository */
    private $empleadoRepository;

    public function __construct(EmpleadoRepository $empleadoRepo)
    {
        $this->empleadoRepository = $empleadoRepo;
    }

    /**
     * Display a listing of the Empleado.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $empleados = $this->empleadoRepository->all();

        return view('empleados.index')
            ->with('empleados', $empleados);
    }

    /**
     * Show the form for creating a new Empleado.
     *
     * @return Response
     */
    public function create(Request $request)
    {   
        $datos = $request->all();
        return view('empleados.create')->with('datos',$datos);
    }

    /**
     * Store a newly created Empleado in storage.
     *
     * @param CreateEmpleadoRequest $request
     *
     * @return Response
     */
    public function store(CreateEmpleadoRequest $request)
    {   
        $id_user = auth()->user()->id;
        $input = $request->all();
        $datos_empleado = ['apynom' => $input['apynom'],
                            'nro_dni' => $input['nro_dni'],
                            'nro_expediente' => $input['nro_expediente'],
                            'nro_legajo' => $input['nro_legajo']
                            ];

        $empleado = $this->empleadoRepository->create($datos_empleado);


        $id_empleado = Empleado::select('id')
                                ->where('nro_dni',$input['nro_dni'])
                                ->orderby('id','desc')
                                ->first();

        if(!isset($input['id_localidad_trabaja'])){
            $input['id_localidad_trabaja'] = null;
            if(!isset($input['id_titulo'])){
                $input['id_titulo'] = null;
            }
        }
        else{
            $input['id_titulo'] = null;
            $input['tiene_capacitacion']= null;
        }

        $datos_empl_rec = [ 'id_categoria_inicial' => $input['id_categoria_inicial'],
                            'id_categoria_ascenso' => $input['id_categoria_ascenso'],
                            'fecha_inicial'  => $input['fecha_inicial'],
                            'fecha_final'  => $input['fecha_final'],
                            'fecha_ascenso' => $input['fecha_ascenso'],
                            'id_localidad_trabaja' => $input['id_localidad_trabaja'],
                            'id_titulo' => $input['id_titulo'],
                            'tiene_capacitacion' => $input['tiene_capacitacion'],
                            'id_empleado' => $id_empleado->id,
                            'id_user' => $id_user
                            ];
        EmpleadoRecategorizacion::create($datos_empl_rec);

        Flash::success('Cálculo guardaro Correctamente.');

        return redirect(route('empleadoRecategorizacions.index'));
    }

    /**
     * Display the specified Empleado.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $empleado = $this->empleadoRepository->find($id);
        $empleadoRecategorizacion = EmpleadoRecategorizacion::where('id_empleado',$empleado->id)->first();
        
        if (empty($empleado)) {
            Flash::error('Empleado not found');

            return redirect(route('empleados.index'));
        }

        return view('empleados.show')->with('empleado', $empleado)
                        ->with('empleadoRecategorizacion',$empleadoRecategorizacion);
    }

    /**
     * Show the form for editing the specified Empleado.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $empleado = $this->empleadoRepository->find($id);
        $empleadoRecategorizacion = EmpleadoRecategorizacion::where('id_empleado',$empleado->id)->first();
        if (empty($empleado)) {
            Flash::error('Empleado not found');

            return redirect(route('empleados.index'));
        }

        return view('empleados.edit')->with('empleado', $empleado)->with('empleadoRecategorizacion',$empleadoRecategorizacion);
    }

    /**
     * Update the specified Empleado in storage.
     *
     * @param int $id
     * @param UpdateEmpleadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmpleadoRequest $request)
    {
        $empleado = $this->empleadoRepository->find($id);
        $empleadoRecategorizacion = EmpleadoRecategorizacion::where('id_empleado', $id)->first();

        if (empty($empleado)) {
            Flash::error('Empleado not found');

            return redirect(route('empleados.index'));
        }

        $empleado = $this->empleadoRepository->update($request->all(), $id);

        Flash::success('Empleado updatedCorrectamente.');

        return redirect(route('empleadoRecategorizacions.edit', $empleadoRecategorizacion->id));
    }

    /**
     * Remove the specified Empleado from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $empleado = $this->empleadoRepository->find($id);
        
        $empleadoRecategorizacion = EmpleadoRecategorizacion::where('id_empleado',$empleado->id)->first();
        
        EmpleadoRecategorizacion::destroy($empleadoRecategorizacion->id);


        if (empty($empleado)) {
            Flash::error('Empleado no encontrado');

            return redirect(route('empleados.index'));
        }

        $this->empleadoRepository->delete($id);

        Flash::success('Cálculo eliminado Correctamente.');

        return redirect(route('empleadoRecategorizacions.index'));
    }

    /**********************************************************************/
    /***************PDF****************************************************/
    /**********************************************************************/

    private function obtener_recategorizacion($id){
        $recategorizacion = EmpleadoRecategorizacion::where('id',$id)->first();
        $data['recategorizacion'] = $recategorizacion;
        //dd($data);
        $pdf = PDF::loadView('/empleados/recategorizacion_pdf', $data);
        return $pdf;
        
    }

    public function descargar_recategorizacion(Request $request){
        $id = $request->id_emp_rec;
        $pdf = $this->obtener_recategorizacion($id);
        $download = $pdf->download("Recategorización N° $id.pdf");
        return $download;
    }
    public function imprimir_recategorizacion(Request $request){
        $id = $request->id_emp_rec;
        $pdf = $this->obtener_recategorizacion($id);
        $download = $pdf->stream("Recategorización N° $id.pdf");
        return $download;
    }
}
