<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePermisoRequest;
use App\Http\Requests\UpdatePermisoRequest;
use App\Repositories\PermisoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Permiso;

class PermisoController extends AppBaseController
{
    /** @var  PermisoRepository */
    private $permisoRepository;

    public function __construct(PermisoRepository $permisoRepo)
    {
        $this->permisoRepository = $permisoRepo;
    }

    /**
     * Display a listing of the Permiso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $permisos = $this->permisoRepository->all();

        return view('permisos.index')
            ->with('permisos', $permisos);
    }

    /**
     * Show the form for creating a new Permiso.
     *
     * @return Response
     */
    public function create()
    {
        return view('permisos.create');
    }

    /**
     * Store a newly created Permiso in storage.
     *
     * @param CreatePermisoRequest $request
     *
     * @return Response
     */
    public function store(CreatePermisoRequest $request)
    {
        $input = $request->all();

        //Agrego los 4 permisos a la tabla $request->name

        $input['name'] = 'ver_'.$request->tabla;
        $input['permiso'] = 'ver';
        $input['tabla'] = $request->tabla;
        $permiso = $this->permisoRepository->create($input);

        $input['name'] = 'crear_'.$request->tabla;
        $input['permiso'] = 'crear';
        $input['tabla'] = $request->tabla;
        $permiso = $this->permisoRepository->create($input);

        $input['name'] = 'editar_'.$request->tabla;
        $input['permiso'] = 'editar';
        $input['tabla'] = $request->tabla;
        $permiso = $this->permisoRepository->create($input);

        $input['name'] = 'eliminar_'.$request->tabla;
        $input['permiso'] = 'eliminar';
        $input['tabla'] = $request->tabla;
        $permiso = $this->permisoRepository->create($input);

        Flash::success('Permiso saved successfully.');

        return redirect(route('permisos.index'));
    }

    /**
     * Display the specified Permiso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            Flash::error('Permiso not found');

            return redirect(route('permisos.index'));
        }

        return view('permisos.show')->with('permiso', $permiso);
    }

    /**
     * Show the form for editing the specified Permiso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            Flash::error('Permiso not found');

            return redirect(route('permisos.index'));
        }

        return view('permisos.edit')->with('permiso', $permiso);
    }

    /**
     * Update the specified Permiso in storage.
     *
     * @param int $id
     * @param UpdatePermisoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePermisoRequest $request)
    {
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            Flash::error('Permiso not found');

            return redirect(route('permisos.index'));
        }

/*        //Obtengo todos los permisos asociados a la tabla a editar
        $explode = explode('_', $permiso->name);
        $ver = 'ver_'.$explode[1];
        $crear = 'crear_'.$explode[1];
        $editar = 'editar_'.$explode[1];
        $eliminar = 'eliminar_'.$explode[1];

        $permisos = Permiso::where('name', $ver)
                            ->orwhere('name', $crear)
                            ->orwhere('name', $editar)
                            ->orwhere('name', $eliminar)
                            ->get();

        $tabla = explode('_', $request->name);
*/
        $input = $request->all();
        $permisos = Permiso::where('tabla', $permiso->tabla)->get();
        //Edito el nombre de la tabla de todos los permisos
        foreach ($permisos as $permisoAnterior) {

            if($permisoAnterior->permiso == 'ver'){
                $input['name'] = 'ver_'.$request->tabla;
                $input['permiso'] = 'ver';
                $input['tabla'] = $request->tabla;
                $this->permisoRepository->update($input, $permisoAnterior->id);    
            }
            else if($permisoAnterior->permiso == 'crear'){
                $input['name'] = 'crear_'.$request->tabla;
                $input['permiso'] = 'crear';
                $input['tabla'] = $request->tabla;
                $this->permisoRepository->update($input, $permisoAnterior->id);    
            }
            else if($permisoAnterior->permiso == 'editar'){
                $input['name'] = 'editar_'.$request->tabla;
                $input['permiso'] = 'editar';
                $input['tabla'] = $request->tabla;
                $this->permisoRepository->update($input, $permisoAnterior->id);    
            }
            else if($permisoAnterior->permiso == 'eliminar'){
                $input['name'] = 'eliminar_'.$request->tabla;
                $input['permiso'] = 'eliminar';
                $input['tabla'] = $request->tabla;
                $this->permisoRepository->update($input, $permisoAnterior->id);    
            }
        }

        Flash::success('Permiso updated successfully.');

        return redirect(route('permisos.index'));
    }

    /**
     * Remove the specified Permiso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            Flash::error('Permiso not found');

            return redirect(route('permisos.index'));
        }

/*        //Obtengo todos los permisos asociados a la tabla a eliminar
        $explode = explode('_', $permiso->name);
        $ver = 'ver_'.$explode[1];
        $crear = 'crear_'.$explode[1];
        $editar = 'editar_'.$explode[1];
        $eliminar = 'eliminar_'.$explode[1];

        $permisos = Permiso::where('name', $ver)
                            ->orwhere('name', $crear)
                            ->orwhere('name', $editar)
                            ->orwhere('name', $eliminar)
                            ->get();*/

        $permisos = Permiso::where('tabla', $permiso->tabla)->get();

        foreach ($permisos as $p) {
            $this->permisoRepository->delete($p->id);
        }

        Flash::success('Permiso deleted successfully.');

        return redirect(route('permisos.index'));
    }
}
