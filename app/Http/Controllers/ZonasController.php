<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateZonasRequest;
use App\Http\Requests\UpdateZonasRequest;
use App\Repositories\ZonasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ZonasController extends AppBaseController
{
    /** @var  ZonasRepository */
    private $zonasRepository;

    public function __construct(ZonasRepository $zonasRepo)
    {
        $this->zonasRepository = $zonasRepo;
    }

    /**
     * Display a listing of the Zonas.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $zonas = $this->zonasRepository->all();

        return view('zonas.index')
            ->with('zonas', $zonas);
    }

    /**
     * Show the form for creating a new Zonas.
     *
     * @return Response
     */
    public function create()
    {
        return view('zonas.create');
    }

    /**
     * Store a newly created Zonas in storage.
     *
     * @param CreateZonasRequest $request
     *
     * @return Response
     */
    public function store(CreateZonasRequest $request)
    {
        $input = $request->all();

        $zonas = $this->zonasRepository->create($input);

        Flash::success('Zonas saved successfully.');

        return redirect(route('zonas.index'));
    }

    /**
     * Display the specified Zonas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $zonas = $this->zonasRepository->find($id);

        if (empty($zonas)) {
            Flash::error('Zonas not found');

            return redirect(route('zonas.index'));
        }

        return view('zonas.show')->with('zonas', $zonas);
    }

    /**
     * Show the form for editing the specified Zonas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $zonas = $this->zonasRepository->find($id);

        if (empty($zonas)) {
            Flash::error('Zonas not found');

            return redirect(route('zonas.index'));
        }

        return view('zonas.edit')->with('zonas', $zonas);
    }

    /**
     * Update the specified Zonas in storage.
     *
     * @param int $id
     * @param UpdateZonasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateZonasRequest $request)
    {
        $zonas = $this->zonasRepository->find($id);

        if (empty($zonas)) {
            Flash::error('Zonas not found');

            return redirect(route('zonas.index'));
        }

        $zonas = $this->zonasRepository->update($request->all(), $id);

        Flash::success('Zonas updated successfully.');

        return redirect(route('zonas.index'));
    }

    /**
     * Remove the specified Zonas from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $zonas = $this->zonasRepository->find($id);

        if (empty($zonas)) {
            Flash::error('Zonas not found');

            return redirect(route('zonas.index'));
        }

        $this->zonasRepository->delete($id);

        Flash::success('Zonas deleted successfully.');

        return redirect(route('zonas.index'));
    }
}
