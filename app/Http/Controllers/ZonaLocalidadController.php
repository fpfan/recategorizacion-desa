<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateZonaLocalidadRequest;
use App\Http\Requests\UpdateZonaLocalidadRequest;
use App\Repositories\ZonaLocalidadRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ZonaLocalidadController extends AppBaseController
{
    /** @var  ZonaLocalidadRepository */
    private $zonaLocalidadRepository;

    public function __construct(ZonaLocalidadRepository $zonaLocalidadRepo)
    {
        $this->zonaLocalidadRepository = $zonaLocalidadRepo;
    }

    /**
     * Display a listing of the ZonaLocalidad.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $zonaLocalidads = $this->zonaLocalidadRepository->all();

        return view('zona_localidads.index')
            ->with('zonaLocalidads', $zonaLocalidads);
    }

    /**
     * Show the form for creating a new ZonaLocalidad.
     *
     * @return Response
     */
    public function create()
    {
        return view('zona_localidads.create');
    }

    /**
     * Store a newly created ZonaLocalidad in storage.
     *
     * @param CreateZonaLocalidadRequest $request
     *
     * @return Response
     */
    public function store(CreateZonaLocalidadRequest $request)
    {
        $input = $request->all();

        $zonaLocalidad = $this->zonaLocalidadRepository->create($input);

        Flash::success('Zona Localidad saved successfully.');

        return redirect(route('zonaLocalidads.index'));
    }

    /**
     * Display the specified ZonaLocalidad.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $zonaLocalidad = $this->zonaLocalidadRepository->find($id);

        if (empty($zonaLocalidad)) {
            Flash::error('Zona Localidad not found');

            return redirect(route('zonaLocalidads.index'));
        }

        return view('zona_localidads.show')->with('zonaLocalidad', $zonaLocalidad);
    }

    /**
     * Show the form for editing the specified ZonaLocalidad.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $zonaLocalidad = $this->zonaLocalidadRepository->find($id);

        if (empty($zonaLocalidad)) {
            Flash::error('Zona Localidad not found');

            return redirect(route('zonaLocalidads.index'));
        }

        return view('zona_localidads.edit')->with('zonaLocalidad', $zonaLocalidad);
    }

    /**
     * Update the specified ZonaLocalidad in storage.
     *
     * @param int $id
     * @param UpdateZonaLocalidadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateZonaLocalidadRequest $request)
    {
        $zonaLocalidad = $this->zonaLocalidadRepository->find($id);

        if (empty($zonaLocalidad)) {
            Flash::error('Zona Localidad not found');

            return redirect(route('zonaLocalidads.index'));
        }

        $zonaLocalidad = $this->zonaLocalidadRepository->update($request->all(), $id);

        Flash::success('Zona Localidad updated successfully.');

        return redirect(route('zonaLocalidads.index'));
    }

    /**
     * Remove the specified ZonaLocalidad from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $zonaLocalidad = $this->zonaLocalidadRepository->find($id);

        if (empty($zonaLocalidad)) {
            Flash::error('Zona Localidad not found');

            return redirect(route('zonaLocalidads.index'));
        }

        $this->zonaLocalidadRepository->delete($id);

        Flash::success('Zona Localidad deleted successfully.');

        return redirect(route('zonaLocalidads.index'));
    }
}
