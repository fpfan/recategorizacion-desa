<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRegimenRequest;
use App\Http\Requests\UpdateRegimenRequest;
use App\Repositories\RegimenRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RegimenController extends AppBaseController
{
    /** @var  RegimenRepository */
    private $regimenRepository;

    public function __construct(RegimenRepository $regimenRepo)
    {
        $this->regimenRepository = $regimenRepo;
    }

    /**
     * Display a listing of the Regimen.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $regimens = $this->regimenRepository->all();

        return view('regimens.index')
            ->with('regimens', $regimens);
    }

    /**
     * Show the form for creating a new Regimen.
     *
     * @return Response
     */
    public function create()
    {
        return view('regimens.create');
    }

    /**
     * Store a newly created Regimen in storage.
     *
     * @param CreateRegimenRequest $request
     *
     * @return Response
     */
    public function store(CreateRegimenRequest $request)
    {
        $input = $request->all();

        $regimen = $this->regimenRepository->create($input);

        Flash::success('Regimen saved successfully.');

        return redirect(route('regimens.index'));
    }

    /**
     * Display the specified Regimen.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $regimen = $this->regimenRepository->find($id);

        if (empty($regimen)) {
            Flash::error('Regimen not found');

            return redirect(route('regimens.index'));
        }

        return view('regimens.show')->with('regimen', $regimen);
    }

    /**
     * Show the form for editing the specified Regimen.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $regimen = $this->regimenRepository->find($id);

        if (empty($regimen)) {
            Flash::error('Regimen not found');

            return redirect(route('regimens.index'));
        }

        return view('regimens.edit')->with('regimen', $regimen);
    }

    /**
     * Update the specified Regimen in storage.
     *
     * @param int $id
     * @param UpdateRegimenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegimenRequest $request)
    {
        $regimen = $this->regimenRepository->find($id);

        if (empty($regimen)) {
            Flash::error('Regimen not found');

            return redirect(route('regimens.index'));
        }

        $regimen = $this->regimenRepository->update($request->all(), $id);

        Flash::success('Regimen updated successfully.');

        return redirect(route('regimens.index'));
    }

    /**
     * Remove the specified Regimen from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $regimen = $this->regimenRepository->find($id);

        if (empty($regimen)) {
            Flash::error('Regimen not found');

            return redirect(route('regimens.index'));
        }

        $this->regimenRepository->delete($id);

        Flash::success('Regimen deleted successfully.');

        return redirect(route('regimens.index'));
    }
}
