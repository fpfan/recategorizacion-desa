<?php
 
namespace App\Http\Controllers;

use App\Http\Requests\CreatePermisoRolRequest;
use App\Http\Requests\UpdatePermisoRolRequest;
use App\Repositories\PermisoRolRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Role;
use App\Models\Permiso;
use App\Models\PermisoRol;

class PermisoRolController extends AppBaseController
{
    /** @var  PermisoRolRepository */
    private $permisoRolRepository;

    public function __construct(PermisoRolRepository $permisoRolRepo)
    {
        $this->permisoRolRepository = $permisoRolRepo;
    }

    /**
     * Display a listing of the PermisoRol.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
       $permisos = Permiso::orderby('id', 'asc')->get();
       $roles = Role::get();

       $permisos_roles = [];
       $i = 0;

       foreach ($roles as $rol) {
            foreach ($permisos as $permiso) {
                $tiene_permiso = PermisoRol::where('role_id', $rol->id)
                                            ->where('permiso_id', $permiso->id)
                                            ->get();
                if(count($tiene_permiso) > 0){
                    $permisos_roles[$rol->name][$i] = ['rol_id' => $rol->id, 'rol' => $rol->name, 'permiso' => $permiso->name, 'tabla' => $permiso->tabla, 'tiene_permiso' => true];
                }
                else{                    
                    $permisos_roles[$rol->name][$i] = ['rol_id' => $rol->id, 'rol' => $rol->name, 'permiso' => $permiso->name, 'tabla' => $permiso->tabla, 'tiene_permiso' => false];
                }
                $i = $i+1;
           }
       }               
        return view('permiso_rols.index')
            ->with('permisos_roles', $permisos_roles)
            ->with('roles', $roles)
            ->with('tabla', NULL)
            ->with('busqueda', $request->busqueda);
    }

    /**
     * Show the form for creating a new PermisoRol.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::orderby('name', 'asc')->pluck('name', 'id');
        $permisos = NULL;

        $permisos = Permiso::select('permisos.tabla')->distinct()->get();

        /*$permiso_rol = PermisoRol::select('permisos.name', 'permiso_rol.id as id_permiso_rol')
                            ->join('permisos', 'permisos.id', 'permiso_rol.permiso_id')
                            ->orderby('permisos.id')
                            ->where('permiso_rol.role_id', 1)
                            ->get();*/

        return view('permiso_rols.create')
                ->with('roles', $roles)
                ->with('permisos', $permisos)
                ->with('tabla', NULL);
    }

    /**
     * Store a newly created PermisoRol in storage.
     *
     * @param CreatePermisoRolRequest $request
     *
     * @return Response
     */
    public function store(CreatePermisoRolRequest $request)
    {
        $input = $request->all();
        //Elimino todos los permisos asociados al usuario;
        PermisoRol::where('role_id', $request->role_id)->delete();

        //Le cargo todos los permisos seleccionados al usuario
        $permisos = Permiso::get();

        $permiso_rol['role_id'] = $request->role_id;
        foreach ($permisos as $permiso) {
            if($input[$permiso->name] == 1){
                $permiso_rol['permiso_id'] = $permiso->id;
                $this->permisoRolRepository->create($permiso_rol);
            }            
        }

        Flash::success('Permiso Rol saved successfully.');

        return redirect(route('permisoRols.index'));
    }

    /**
     * Display the specified PermisoRol.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $permisoRol = $this->permisoRolRepository->find($id);

        if (empty($permisoRol)) {
            Flash::error('Permiso Rol not found');

            return redirect(route('permisoRols.index'));
        }

        return view('permiso_rols.show')->with('permisoRol', $permisoRol);
    }

    /**
     * Show the form for editing the specified PermisoRol.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)//id = role_id
    {
       $permisos = Permiso::orderby('id', 'asc')->get();
       $roles = Role::where('id', $id)->get();

       $permisos_roles = [];
       $i = 0;

       foreach ($roles as $rol) {
            foreach ($permisos as $permiso) {
                $tiene_permiso = PermisoRol::where('role_id', $rol->id)
                                            ->where('permiso_id', $permiso->id)
                                            ->get();
                if(count($tiene_permiso) > 0){
                    $permisos_roles[$rol->name][$i] = ['rol_id' => $rol->id, 'rol' => $rol->name, 'permiso' => $permiso->name, 'tabla' => $permiso->tabla, 'tiene_permiso' => true];
                }
                else{                    
                    $permisos_roles[$rol->name][$i] = ['rol_id' => $rol->id, 'rol' => $rol->name, 'permiso' => $permiso->name, 'tabla' => $permiso->tabla, 'tiene_permiso' => false];
                }
                $i = $i+1;
           }
       }               
        return view('permiso_rols.edit')
            ->with('permisos_roles', $permisos_roles)
            ->with('roles', $roles)
            ->with('tabla', NULL);
    }

    /**
     * Update the specified PermisoRol in storage.
     *
     * @param int $id
     * @param UpdatePermisoRolRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePermisoRolRequest $request)
    {
        $permisoRol = $this->permisoRolRepository->find($id);

        if (empty($permisoRol)) {
            Flash::error('Permiso Rol not found');

            return redirect(route('permisoRols.index'));
        }
        $input = $request->all();
        PermisoRol::where('role_id', $request->role_id)->delete();

        //Le cargo todos los permisos seleccionados al usuario
        $permisos = Permiso::get();

        $permiso_rol['role_id'] = $request->role_id;
        foreach ($permisos as $permiso) {
            if($input[$permiso->name] == 1){
                $permiso_rol['permiso_id'] = $permiso->id;
                $this->permisoRolRepository->create($permiso_rol);
            }            
        }

        Flash::success('Permiso Rol se ha actualizado exitosamente.');

        return redirect(route('permisoRols.index'));
    }

    /**
     * Remove the specified PermisoRol from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $permisoRol = $this->permisoRolRepository->find($id);

        if (empty($permisoRol)) {
            Flash::error('Permiso Rol not found');

            return redirect(route('permisoRols.index'));
        }

        $this->permisoRolRepository->delete($id);

        Flash::success('Permiso Rol deleted successfully.');

        return redirect(route('permisoRols.index'));
    }
}
