<?php

namespace App\Http\Controllers; 

use App\Http\Requests\CreateEmpleadoRecategorizacionRequest;
use App\Http\Requests\UpdateEmpleadoRecategorizacionRequest;
use App\Repositories\EmpleadoRecategorizacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Regimen;
use App\Models\Localidades;
use App\Models\Titulo;

use App\Models\Agrupamiento;
use App\Models\Categoria;
use App\Models\EmpleadoRecategorizacion;



class EmpleadoRecategorizacionController extends AppBaseController
{
    /** @var  EmpleadoRecategorizacionRepository */
    private $empleadoRecategorizacionRepository;

    public function __construct(EmpleadoRecategorizacionRepository $empleadoRecategorizacionRepo)
    {
        $this->empleadoRecategorizacionRepository = $empleadoRecategorizacionRepo;
    }

    /**
     * Display a listing of the EmpleadoRecategorizacion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $empleadoRecategorizacions = EmpleadoRecategorizacion::select('empleado_recategorizacion.*')->join('empleados', 'empleado_recategorizacion.id_empleado', 'empleados.id')
                        ->where('apynom', 'like', '%'.$request->busqueda.'%')
                        ->orWhere('nro_dni', $request->busqueda)
                        ->orWhere('nro_expediente', $request->busqueda) 
                        ->paginate(20);

        return view('empleado_recategorizacions.index')
            ->with('empleadoRecategorizacions', $empleadoRecategorizacions);
    }

    /**
     * Show the form for creating a new EmpleadoRecategorizacion.
     *
     * @return Response
     */
    public function create()
    {
        $regimenes = Regimen::get();
        $localidades = Localidades::orderby('descripcion', 'asc')->get();
        $titulos = Titulo::get();
        $agrupamientos = Agrupamiento::get();
        
        return view('empleado_recategorizacions.create')
                ->with('regimenes',$regimenes)
                ->with('localidades',$localidades)
                ->with('titulos',$titulos)
                ->with('agrupamientos', null)
                ->with('categorias', null);
    }

    /**
     * Store a newly created EmpleadoRecategorizacion in storage.
     *
     * @param CreateEmpleadoRecategorizacionRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $empleadoRecategorizacion = $request->all();
        //dd($empleadoRecategorizacion);
        return view('empleados.create')
              ->with('empleadoRecategorizacion',$empleadoRecategorizacion);
        /*$empleadoRecategorizacion = $this->empleadoRecategorizacionRepository->create($input);

        Flash::success('Empleado Recategorizacion saved successfully.');

        return redirect(route('empleadoRecategorizacions.show', [$empleadoRecategorizacion->id]));*/


    }

    public function calcular(Request $request){
        $input = $request->all();
        /**************VALIDAR DATOS*************************/
        $validatedData = $request->validate([
            'id_categoria_inicial' => 'required',
            'fecha_inicial' => 'required',
            'fecha_final' => 'required | date_format:Y-m-d',
            ],
            ['fecha_inicial.required' => 'Fecha Inicial es requerido', 
             'id_categoria_inicial.required' => 'Regimen, Agrupamiento y Categoria son requeridos', 
             'fecha_final.required' => 'Fecha Final es requerido',
             'fecha_final.date_format' => 'Fecha Final tiene un formato invalido. Pruebe AAAA-mm-dd']
        );

        $categoria_inicial = Categoria::where('id', $request->id_categoria_inicial)->first();
        if($categoria_inicial->cod_reg == 2){
            $validatedData = $request->validate([
                'tiene_capacitacion' => 'required',
            ],
            ['tiene_capacitacion.required' => 'Tiene capacitacion es requerido']
            );

            if($categoria_inicial->id_agrupamiento == 3 || $categoria_inicial->id_agrupamiento ==6){
                    $validatedData = $request->validate([
                        'id_titulo' => 'required',
                    ],
                    ['id_titulo.required' => 'Titulo es requerido']
                    );
            }

        }
        else{
            $validatedData = $request->validate([
                'id_localidad_trabaja' => 'required',
            ],
            ['id_localidad_trabaja.required' => 'Localidad es requerido']
            );
        }

        /***********************************************/

        $input = $this->promocion($input);

        $categoria_inicial = Categoria::where('id', $input['id_categoria_inicial'])->first();
        $categoria_ascenso = Categoria::where('id', $input['id_categoria_ascenso'])->first();

        if($request->id_regimen == 2){
            if($request->id_agrupamiento == 3 || $request->id_agrupamiento == 6){
                $titulo = Titulo::where('id', $input['id_titulo'])->first();
            }
            else{
                $titulo = null;
            }
            $tiene_capacitacion = $input['tiene_capacitacion'];
            $localidad = null;
        }
        else{
            $localidad = Localidades::where('id', $input['id_localidad_trabaja'])->first();
            $titulo = null;
            $tiene_capacitacion = null;
        }
        
        $arreglo = ['id' => $input['id'],
                      'btn' => $input['btn'],
                      'categoria_inicial' => $categoria_inicial,
                      'categoria_ascenso' => $categoria_ascenso,
                      'fecha_inicial' => $input['fecha_inicial'],
                      'fecha_final' => $input['fecha_final'],
                      'fecha_ascenso' => $input['fecha_ascenso'],
                      'tiene_capacitacion' => $tiene_capacitacion,
                      'titulo' => $titulo,
                      'localidad_trabaja' => $localidad  
                    ];
        return view('empleado_recategorizacions.show_calculo')->with('empleadoRecategorizacion', $arreglo);
    }

    /**
     * Display the specified EmpleadoRecategorizacion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $empleadoRecategorizacion = $this->empleadoRecategorizacionRepository->find($id);

        if (empty($empleadoRecategorizacion)) {
            Flash::error('Recategorización no encontrada');

            return redirect(route('empleadoRecategorizacions.index'));
        }

        return view('empleado_recategorizacions.show')->with('empleadoRecategorizacion', $empleadoRecategorizacion);
    }

    /**
     * Show the form for editing the specified EmpleadoRecategorizacion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $empleadoRecategorizacion = $this->empleadoRecategorizacionRepository->find($id);

        if (empty($empleadoRecategorizacion)) {
            Flash::error('Recategorización no encontrada');

            return redirect(route('empleadoRecategorizacions.index'));
        }

        $regimenes = Regimen::whereIn('id', [2,3])->get();
        $localidades = Localidades::orderby('descripcion', 'asc')->get();
        $titulos = Titulo::get();
        
        $agrupamientos = Agrupamiento::where('id_regimen', $empleadoRecategorizacion->Categoria_inicial->Agrupamiento->Regimen->id)->get();
        $categorias = Categoria::where('id_agrupamiento', $empleadoRecategorizacion->Categoria_inicial->Agrupamiento->id)->orderby('descripcion', 'asc')->get();

        if (empty($empleadoRecategorizacion)) {
            Flash::error('Empleado Recategorizacion no encontrado');

            return redirect(route('empleadoRecategorizacions.index'));
        }

        return view('empleado_recategorizacions.edit')
                ->with('empleadoRecategorizacion', $empleadoRecategorizacion)
                ->with('regimenes',$regimenes)
                ->with('localidades',$localidades)
                ->with('titulos',$titulos)
                ->with('agrupamientos', $agrupamientos)
                ->with('categorias', $categorias);
    }

    /**
     * Update the specified EmpleadoRecategorizacion in storage.
     *
     * @param int $id
     * @param UpdateEmpleadoRecategorizacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmpleadoRecategorizacionRequest $request)
    {
        $empleadoRecategorizacion = $this->empleadoRecategorizacionRepository->find($id);

        if (empty($empleadoRecategorizacion)) {
            Flash::error('Recategorización no encontrada');

            return redirect(route('empleadoRecategorizacions.index'));
        }

        $empleadoRecategorizacion = $this->empleadoRecategorizacionRepository->update($request->all(), $id);

        Flash::success('Recategorización actualizada correctamente.');

        return redirect(route('empleadoRecategorizacions.index'));
    }

    /**
     * Remove the specified EmpleadoRecategorizacion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $empleadoRecategorizacion = $this->empleadoRecategorizacionRepository->find($id);

        if (empty($empleadoRecategorizacion)) {
            Flash::error('Recategorización no encontrada');

            return redirect(route('empleadoRecategorizacions.index'));
        }

        $empleado = Empleado::where('id', $empleadoRecategorizacion->id_empleado)->delete();
        $this->empleadoRecategorizacionRepository->delete($id);

        Flash::success('La recategorización ha sido eliminada.');

        return redirect(route('empleadoRecategorizacions.index'));
    }

    public function promocion($input){  
        //id_categoria_inicial, fecha_inicial, tiene_capacitacion, id_localidad_trabaja
        $categoria_inicial = Categoria::where('id',$input['id_categoria_inicial'])->first();
            
        $fecha_actual = substr(date('Y-m-d'),0,10);
        $fecha_inicial = substr($input['fecha_inicial'], 0, 10);
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_inicial);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $input['fecha_final']);

        $cantidad_anios_total = $to->diffInYears($from);

        $regimen = $categoria_inicial->cod_reg;

        if($regimen == 3){//Ley 1904
            $input = $this->categoria_siguiente_salud($input, $categoria_inicial); 
        }
        else{
            $input = $this->categoria_siguiente($cantidad_anios_total, $input, $categoria_inicial); 
        }

        //dd($input);
        return $input;
    }

    private function categoria_siguiente($cantidad_anios_total, $input, $categoria){
        //dd($categoria);
        $i = $cantidad_anios_total;
        $encontro = false; 
        $hasta = $input['fecha_final'];

        $categoria_inicial = $categoria;
        $fecha_inicial = $input['fecha_inicial'];

        $fecha_ascenso = $this->get_fecha_inicio_promocion($input['fecha_inicial']);

        while($i >= 0 && $encontro == false){
            $i = $i-$categoria->anios_promocion;

            if($i >= 0){
                $categoria_siguiente = Categoria::where('id', $categoria->siguiente)->first();

                if (($input['id_titulo'] >= $categoria_siguiente->titulo_minimo) && 
                    (
                        ($categoria_siguiente->capacitacion == 1 && $input['tiene_capacitacion'] == 1)
                            XOR $categoria_siguiente->capacitacion == 0)
                    )
                    {
                        //dd($categoria_siguiente);
                        $categoria_vieja = $categoria;
                        $fecha_ascenso_viejo = $fecha_ascenso;

                        $categoria = $categoria_siguiente;
                        $anios = $categoria->anios_promocion;
                        $nuevafecha = strtotime ( "+$anios years" , strtotime ( $fecha_ascenso)) ;
                        $fecha_ascenso = date ('Y-m-d', $nuevafecha);

                        if(($fecha_ascenso > $hasta) || ($categoria_vieja->id == $categoria->id)){
                            $categoria = $categoria_vieja;
                            $fecha_ascenso = $fecha_ascenso_viejo;
                            $encontro = true;
                        }

                    }
                else
                    {  
                        $encontro = true;
                }

            }

        }

        if($categoria->id == $categoria_inicial->id){
            $input['id_categoria_ascenso'] = $categoria_inicial->id;
            $input['fecha_ascenso'] = $fecha_inicial;       
        }
        else{
            $input['id_categoria_ascenso'] = $categoria->id;
            $input['fecha_ascenso'] = $fecha_ascenso;   
        }

        return $input;
    }


    private function categoria_siguiente_salud($input, $categoria){
        $encontro = false; 
        $hasta = $input['fecha_final'];

        $categoria_inicial = $categoria;
        $fecha_inicial = $input['fecha_inicial'];
        $fecha_final= $input['fecha_final'];

        $fecha_ascenso = $this->get_fecha_inicio_promocion($input['fecha_inicial']);

        $localidad = Localidades::where('id', $input['id_localidad_trabaja'])->first();

        if($localidad->inhospita == true){ //linea sur
            $anios_recat = 4;
        }
        else{
            $anios_recat = 5;
        }

        $encontro = false; 
        $i = 0;
        while($encontro == false){
            $i++;
            
            $nuevafecha = strtotime ( "+$anios_recat years" , strtotime ( $fecha_ascenso));
            $fecha_ascenso = date ('Y-m-d', $nuevafecha);

            if($fecha_ascenso <= $fecha_final){
                $categoria_siguiente = Categoria::where('id', $categoria->siguiente)->first();
                if($categoria->id == $categoria_siguiente->id){
                    if($i == 1){
                        $fecha_ascenso = $fecha_inicial;
                    }
                    else{
                        $nuevafecha = strtotime ( "-$anios_recat years" , strtotime ( $fecha_ascenso));
                        $fecha_ascenso = date ('Y-m-d', $nuevafecha);
                    }
                    $encontro = true;
                }
                else{
                    $categoria = $categoria_siguiente;
                }
            }
            else{
                $nuevafecha = strtotime ( "-$anios_recat years" , strtotime ( $fecha_ascenso));
                $fecha_ascenso = date ('Y-m-d', $nuevafecha);
                $encontro = true;
            }
        }

        $input['id_categoria_ascenso'] = $categoria;

        if($categoria->id == $categoria_inicial->id){
            $input['id_categoria_ascenso'] = $categoria_inicial->id;
            $input['fecha_ascenso'] = $fecha_inicial;       
        }
        else{
            $input['id_categoria_ascenso'] = $categoria->id;
            $input['fecha_ascenso'] = $fecha_ascenso;   
        }

        return $input;
    }

    private function get_fecha_inicio_promocion($fecha_promocion){
        $explode = explode('-', substr($fecha_promocion,0,10));
        if($explode[2] != 1){//dia
            if($explode[1] < 12){//mes 
                $mes = $explode[1]+1;
                $anio = $explode[0];
            }
            else{
                $mes = 1;
                $anio = $explode[0]+1;
            }
            $fecha = "$anio-$mes-01";
        }
        else{
           $fecha = $fecha_promocion; 
        }
        /*if($fecha > $fecha_final){
            $anio = $anio-1;
            $fecha = "$anio-$mes-01"
        }*/
        return $fecha;
    }

    public function getAgrupamientos($id){// id = id_reg;id_op;id_postulante
        $agrupamientos = Agrupamiento::where('id_regimen', $id)->orderby('desc_agrup','asc')->get();
        return view('empleado_recategorizacions.ajaxAgrupamientos')
                ->with('agrupamientos',$agrupamientos);
    }

     public function getCategorias($id){// id = id;op;id_postulante
        $categorias = Categoria::where('id_agrupamiento', $id)->get();
        return view('empleado_recategorizacions.ajaxCategorias')
                ->with('categorias',$categorias);
    }

}
