<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\PermisoRol;
use App\Models\Permiso;
use App\User;
use App\Role;

class CheckPermiso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permiso)
    {
        if (Auth::check()) {
                $usuario = User::where('id', $request->user()->id)->first();
                $rol = Role::where('id', $usuario->id_rol)->first();
                $permisos = Permiso::where('name', $permiso)->first();
                $tiene_permiso = PermisoRol::where('permiso_id', $permisos->id)
                                                    ->where('role_id', $rol->id)
                                                    ->first();
                if ($tiene_permiso == null) {
                        abort(401);
                }
                else{
                    return $next($request);
                }
        }
        return route('login'); 
    }
}
