<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckActivo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(auth()->user()->activo){
                //dd(1);
                return $next($request);
            }
            else{
                //dd(2);
                return redirect(route('cambiar_contrasena'));
            }
        }
        else{
            //dd(3);
            return route('login');
        }
    }
}
