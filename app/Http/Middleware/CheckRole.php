<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\User;
use App\Role;
use App\Models\Permiso;
use App\Models\PermisoRol;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::check()) {
            $usuario = User::where('id', $request->user()->id)->first();
            $rol = Role::where('id', $usuario->id_rol)->first();

            if ($rol->name == 'administrador' || $rol->name == 'administrador_aux') {
                return $next($request);
            }
            abort(401);
        }
        else{
            return route('login'); 
        }
    }
}
