<?php

namespace App\Repositories;

use App\Models\Localidades;
use App\Repositories\BaseRepository;

/**
 * Class LocalidadesRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:21 pm UTC
*/

class LocalidadesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'id_provincia'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Localidades::class;
    }
}
