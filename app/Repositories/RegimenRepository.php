<?php

namespace App\Repositories;

use App\Models\Regimen;
use App\Repositories\BaseRepository;

/**
 * Class RegimenRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:21 pm UTC
*/

class RegimenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reg',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Regimen::class;
    }
}
