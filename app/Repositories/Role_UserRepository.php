<?php

namespace App\Repositories;

use App\Models\Role_User;
use App\Repositories\BaseRepository;

/**
 * Class Role_UserRepository
 * @package App\Repositories
 * @version May 24, 2019, 11:47 am UTC
*/

class Role_UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'role_id',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Role_User::class;
    }
}
