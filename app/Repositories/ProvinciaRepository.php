<?php

namespace App\Repositories;

use App\Models\Provincia;
use App\Repositories\BaseRepository;

/**
 * Class ProvinciaRepository
 * @package App\Repositories
 * @version October 16, 2019, 12:00 pm UTC
*/

class ProvinciaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Provincia::class;
    }
}
