<?php

namespace App\Repositories;

use App\Models\Titulo--fromTable;
use App\Repositories\BaseRepository;

/**
 * Class Titulo--fromTableRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:45 pm UTC
*/

class Titulo--fromTableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Titulo--fromTable::class;
    }
}
