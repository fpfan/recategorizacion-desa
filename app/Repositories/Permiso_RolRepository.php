<?php

namespace App\Repositories;

use App\Models\Permiso_Rol;
use App\Repositories\BaseRepository;

/**
 * Class Permiso_RolRepository
 * @package App\Repositories
 * @version June 26, 2019, 2:49 pm UTC
*/

class Permiso_RolRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'role_id',
        'permisos_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permiso_Rol::class;
    }
}
