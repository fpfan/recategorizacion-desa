<?php

namespace App\Repositories;

use App\Models\Zonas;
use App\Repositories\BaseRepository;

/**
 * Class ZonasRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:21 pm UTC
*/

class ZonasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Zonas::class;
    }
}
