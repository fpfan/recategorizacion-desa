<?php

namespace App\Repositories;

use App\Models\EmpleadoRecategorizacion;
use App\Repositories\BaseRepository;

/**
 * Class EmpleadoRecategorizacionRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:21 pm UTC
*/

class EmpleadoRecategorizacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_empleado',
        'id_categoria_inicial',
        'id_categoria_ascenso',
        'fecha_inicial',
        'fecha_ascenso',
        'tiene_capacitacion',
        'id_localidad_trabaja',
        'id_user'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EmpleadoRecategorizacion::class;
    }
}
