<?php

namespace App\Repositories;

use App\Models\Agrupamiento;
use App\Repositories\BaseRepository;

/**
 * Class AgrupamientoRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:21 pm UTC
*/

class AgrupamientoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_regimen',
        'cod_reg',
        'cod_agrup',
        'desc_agrup'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agrupamiento::class;
    }
}
