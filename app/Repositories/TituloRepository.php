<?php

namespace App\Repositories;

use App\Models\Titulo;
use App\Repositories\BaseRepository;

/**
 * Class TituloRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:45 pm UTC
*/

class TituloRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nivel',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Titulo::class;
    }
}
