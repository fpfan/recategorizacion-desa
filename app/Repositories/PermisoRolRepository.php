<?php

namespace App\Repositories;

use App\Models\PermisoRol;
use App\Repositories\BaseRepository;

/**
 * Class PermisoRolRepository
 * @package App\Repositories
 * @version June 27, 2019, 5:47 pm UTC
*/

class PermisoRolRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'permiso_id',
        'role_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PermisoRol::class;
    }
}
