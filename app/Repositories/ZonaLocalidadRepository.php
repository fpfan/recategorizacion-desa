<?php

namespace App\Repositories;

use App\Models\ZonaLocalidad;
use App\Repositories\BaseRepository;

/**
 * Class ZonaLocalidadRepository
 * @package App\Repositories
 * @version October 15, 2019, 2:21 pm UTC
*/

class ZonaLocalidadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_zona',
        'id_localidad'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ZonaLocalidad::class;
    }
}
