<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Zonas
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection zonaLocalidads
 * @property integer codigo
 * @property string descripcion
 */
class Zonas extends Model
{
    use SoftDeletes;

    public $table = 'zonas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'codigo',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigo' => 'integer',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'codigo' => 'required',
        'descripcion' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function zonaLocalidads()
    {
        return $this->hasMany(\App\Models\ZonaLocalidad::class, 'id_zona');
    }
}
