<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Localidades
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection zonaLocalidads
 * @property string descripcion
 * @property integer id_provincia
 */
class Localidades extends Model
{
    use SoftDeletes;

    public $table = 'localidades';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'descripcion',
        'id_provincia',
        'inhospita'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descripcion' => 'string',
        'id_provincia' => 'integer',
        'inhospita' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required',
        'id_provincia' => 'required',
        'inhospita' => 'required'
    ];

    public function Provincia()
    {
        return $this->belongsTo(\App\Models\Provincia::class, 'id_provincia');
    }
}
