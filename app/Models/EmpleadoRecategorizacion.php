<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EmpleadoRecategorizacion
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property integer id_empleado
 * @property integer id_categoria_inicial
 * @property integer id_categoria_ascenso
 * @property string fecha_inicial
 * @property string fecha_ascenso
 * @property boolean tiene_capacitacion
 * @property integer id_localidad_trabaja
 * @property integer id_user
 */
class EmpleadoRecategorizacion extends Model
{
    use SoftDeletes;

    public $table = 'empleado_recategorizacion';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_empleado',
        'id_categoria_inicial',
        'id_categoria_ascenso',
        'fecha_inicial',
        'fecha_final',
        'fecha_ascenso',
        'tiene_capacitacion',
        'id_localidad_trabaja',
        'id_titulo',
        'id_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_empleado' => 'integer',
        'id_categoria_inicial' => 'integer',
        'id_categoria_ascenso' => 'integer',
        'fecha_inicial' => 'date',
        'fecha_final' => 'date',
        'fecha_ascenso' => 'date',
        'tiene_capacitacion' => 'boolean',
        'id_localidad_trabaja' => 'integer',
        'id_titulo' => 'integer',
        'id_user' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        /*'id_empleado' => 'required',
        'id_categoria_inicial' => 'required',
        'id_categoria_ascenso' => 'required',
        'fecha_inicial' => 'required',
        'fecha_ascenso' => 'required',
        'tiene_capacitacion' => 'required',
        'id_localidad_trabaja' => 'required',
        'id_user' => 'required'*/
    ];

    public function Categoria_inicial()
    {
        return $this->belongsTo(\App\Models\Categoria::class, 'id_categoria_inicial');
    }
    public function Categoria_ascenso()
    {
        return $this->belongsTo(\App\Models\Categoria::class, 'id_categoria_ascenso');
    }
    public function Localidad()
    {
        return $this->belongsTo(\App\Models\Localidades::class, 'id_localidad_trabaja');
    }
    public function Titulo()
    {
        return $this->belongsTo(\App\Models\Titulo::class, 'id_titulo');
    }
    public function User()
    {
        return $this->belongsTo(\App\User::class, 'id_user');
    }
     public function Empleado()
    {
        return $this->belongsTo(\App\Models\Empleado::class, 'id_empleado');
    }


    
}
