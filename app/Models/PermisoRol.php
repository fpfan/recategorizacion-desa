<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PermisoRol
 * @package App\Models
 * @version June 27, 2019, 5:47 pm UTC
 *
 * @property integer id
 * @property integer permiso_id
 * @property integer role_id
 */
class PermisoRol extends Model
{

    public $table = 'permiso_rol';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'permiso_id',
        'role_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'permiso_id' => 'integer',
        'role_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    public function role()
    {
        return $this->belongsTo(\App\Role::class, 'role_id');
    }

    public function permiso()
    {
        return $this->belongsTo(\App\Models\Permiso::class, 'permiso_id');
    }
}
