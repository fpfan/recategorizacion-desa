<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Agrupamiento
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property \App\Models\RegimenLaboral idRegimen
 * @property \Illuminate\Database\Eloquent\Collection categorias
 * @property integer id_regimen
 * @property integer cod_reg
 * @property integer cod_agrup
 * @property string desc_agrup
 */
class Agrupamiento extends Model
{
    use SoftDeletes;

    public $table = 'agrupamientos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_regimen',
        'cod_reg',
        'cod_agrup',
        'desc_agrup'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_regimen' => 'integer',
        'cod_reg' => 'integer',
        'cod_agrup' => 'integer',
        'desc_agrup' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_regimen' => 'required',
        'cod_reg' => 'required',
        'cod_agrup' => 'required',
        'desc_agrup' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Regimen()
    {
        return $this->belongsTo(\App\Models\Regimen::class, 'id_regimen');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
}
