<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Titulo
 * @package App\Models
 * @version October 15, 2019, 2:45 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection empleadoRecategorizacions
 * @property \Illuminate\Database\Eloquent\Collection empleadoRecategorizacionBkps
 * @property integer nivel
 * @property string descripcion
 */
class Titulo extends Model
{
    use SoftDeletes;

    public $table = 'titulos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nivel',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nivel' => 'integer',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function empleadoRecategorizacions()
    {
        return $this->hasMany(\App\Models\EmpleadoRecategorizacion::class, 'id_titulo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function empleadoRecategorizacionBkps()
    {
        return $this->hasMany(\App\Models\EmpleadoRecategorizacionBkp::class, 'id_titulo');
    }
}
