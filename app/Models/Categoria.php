<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Categoria
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property \App\Models\Agrupamiento idAgrupamiento
 * @property integer cod_reg
 * @property integer cod_agrup
 * @property integer cod_cat
 * @property boolean inicial
 * @property integer siguiente
 * @property string descripcion
 * @property integer titulo_minimo
 * @property boolean capacitacion
 * @property integer anios_promocion
 * @property integer id_agrupamiento
 */
class Categoria extends Model
{
    use SoftDeletes;

    public $table = 'categorias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'cod_reg',
        'cod_agrup',
        'cod_cat',
        'inicial',
        'siguiente',
        'descripcion',
        'titulo_minimo',
        'capacitacion',
        'anios_promocion',
        'id_agrupamiento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cod_reg' => 'integer',
        'cod_agrup' => 'integer',
        'cod_cat' => 'integer',
        'inicial' => 'boolean',
        'siguiente' => 'integer',
        'descripcion' => 'string',
        'titulo_minimo' => 'integer',
        'capacitacion' => 'boolean',
        'anios_promocion' => 'integer',
        'id_agrupamiento' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cod_reg' => 'required',
        'cod_agrup' => 'required',
        'inicial' => 'required',
        'descripcion' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Agrupamiento()
    {
        return $this->belongsTo(\App\Models\Agrupamiento::class, 'id_agrupamiento');
    }
}
