<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ZonaLocalidad
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property \App\Models\Localidade idLocalidad
 * @property \App\Models\Zona idZona
 * @property integer id_zona
 * @property integer id_localidad
 */
class ZonaLocalidad extends Model
{
    use SoftDeletes;

    public $table = 'zona_localidad';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_zona',
        'id_localidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_zona' => 'integer',
        'id_localidad' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_zona' => 'required',
        'id_localidad' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Localidad()
    {
        return $this->belongsTo(\App\Models\Localidades::class, 'id_localidad');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Zona()
    {
        return $this->belongsTo(\App\Models\Zona::class, 'id_zona');
    }
}
