<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Permiso
 * @package App\Models
 * @version June 27, 2019, 5:46 pm UTC
 *
 * @property integer name
 */
class Permiso extends Model
{
    use SoftDeletes;

    public $table = 'permisos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'tabla',
        'permiso'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    public function roles()
    {
    return $this
        ->belongsToMany('App\Role')
        ->withTimestamps();
    }
    
}
