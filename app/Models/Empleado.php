<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Empleado
 * @package App\Models
 * @version October 15, 2019, 2:21 pm UTC
 *
 * @property string apynom
 * @property integer nro_dni
 * @property string nro_cuil
 * @property integer nro_legajo
 */
class Empleado extends Model
{
    use SoftDeletes;

    public $table = 'empleados';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'apynom',
        'nro_dni',
        'nro_expediente',
        'nro_legajo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'apynom' => 'string',
        'nro_dni' => 'integer',
        'nro_expediente' => 'string',
        'nro_legajo' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'apynom' => 'required',
        'nro_dni' => 'required'
        /*'nro_expediente' => 'required',
        'nro_legajo' => 'required'*/
    ];

    
}
