<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permiso;
use App\Models\PermisoRol;

class Role extends Model
{
	protected $fillable = [
        'name', 'description'
    ];

    public static $rules = [
        'name' => 'required',
    ];

    public function users()
	{
	return $this->belongsToMany(User::class)->withTimestamps();
	}

    public function permisos()
    {
        return $this->belongsToMany(Permiso::class)->withTimestamps();
    }

    public function hasPermiso($permiso)
    {
        $role_id = auth()->user()->role->id;

        $permiso = Permiso::where('name', $permiso)->first();
        
        $hasPermiso = false;

        if($permiso != NULL){
            $permisoRol = PermisoRol::where('role_id', $role_id)->where('permiso_id', $permiso->id)->get();

            if(count($permisoRol) > 0)
                $hasPermiso = true;
        }
        return $hasPermiso;
    }

}
