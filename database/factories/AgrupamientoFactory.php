<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Agrupamiento;
use Faker\Generator as Faker;

$factory->define(Agrupamiento::class, function (Faker $faker) {

    return [
        'id_regimen' => $faker->randomDigitNotNull,
        'cod_reg' => $faker->randomDigitNotNull,
        'cod_agrup' => $faker->randomDigitNotNull,
        'desc_agrup' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
