<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Regimen;
use Faker\Generator as Faker;

$factory->define(Regimen::class, function (Faker $faker) {

    return [
        'reg' => $faker->randomDigitNotNull,
        'descripcion' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
