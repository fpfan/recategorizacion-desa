<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Titulo;
use Faker\Generator as Faker;

$factory->define(Titulo::class, function (Faker $faker) {

    return [
        'nivel' => $faker->randomDigitNotNull,
        'descripcion' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
