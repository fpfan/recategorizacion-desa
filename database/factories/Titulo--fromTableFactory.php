<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Titulo--fromTable;
use Faker\Generator as Faker;

$factory->define(Titulo--fromTable::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
