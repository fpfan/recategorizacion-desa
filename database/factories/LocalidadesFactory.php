<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Localidades;
use Faker\Generator as Faker;

$factory->define(Localidades::class, function (Faker $faker) {

    return [
        'descripcion' => $faker->word,
        'id_provincia' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
