<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EmpleadoRecategorizacion;
use Faker\Generator as Faker;

$factory->define(EmpleadoRecategorizacion::class, function (Faker $faker) {

    return [
        'id_empleado' => $faker->randomDigitNotNull,
        'id_categoria_inicial' => $faker->randomDigitNotNull,
        'id_categoria_ascenso' => $faker->randomDigitNotNull,
        'fecha_inicial' => $faker->word,
        'fecha_ascenso' => $faker->word,
        'tiene_capacitacion' => $faker->word,
        'id_localidad_trabaja' => $faker->randomDigitNotNull,
        'id_user' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
