<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Categoria;
use Faker\Generator as Faker;

$factory->define(Categoria::class, function (Faker $faker) {

    return [
        'cod_reg' => $faker->randomDigitNotNull,
        'cod_agrup' => $faker->randomDigitNotNull,
        'cod_cat' => $faker->randomDigitNotNull,
        'inicial' => $faker->word,
        'siguiente' => $faker->randomDigitNotNull,
        'descripcion' => $faker->word,
        'titulo_minimo' => $faker->randomDigitNotNull,
        'capacitacion' => $faker->word,
        'anios_promocion' => $faker->randomDigitNotNull,
        'id_agrupamiento' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
