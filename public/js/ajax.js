        function getAgrupamientos(ruta){
            var id = ($('select[name=id_regimen]').val()); 
            if(id == 3){
                document.getElementById("localidad").style.display = 'block';
                document.getElementById("titulo").style.display = 'none';
                document.getElementById("tiene_cap").style.display = 'none';
                $('#id_localidad_trabaja').attr("required", "true");
                $('#id_titulo').removeAttr("required");
                $('#tiene_capacitacion').removeAttr("required");
            }
            else{
                document.getElementById("localidad").style.display = 'none';
                document.getElementById("titulo").style.display = 'none';
                document.getElementById("tiene_cap").style.display = 'block';
                $('#id_localidad_trabaja').removeAttr("required", "true");
                $('#id_titulo').removeAttr("required");
                $('#tiene_capacitacion').attr("required", "true");
            }

            $.ajax({
                url:   ruta+'/getAgrupamientos/'+id,
                type:  'get', //método de envio
                beforeSend: function () {
                        $("#agrupamiento_ajax").html("Procesando, espere por favor...");
                },
                success:  function (response) { 
                        $("#agrupamiento_ajax").html(response);
                }
            });
        }
        function getCategorias(ruta){
            var id = ($('select[name=id_agrupamiento]').val());

            if(id == 3 || id == 6){
                document.getElementById("titulo").style.display = 'block';
                $('#id_titulo').attr("required", "true");
            }
            else{
                document.getElementById("titulo").style.display = 'none';
                $('#id_titulo').removeAttr("required");
            }
            
            $.ajax({
                url:   ruta+'/getCategorias/'+id,
                type:  'get', //método de envio
                beforeSend: function () {
                        $("#categoria_ajax").html("Procesando, espere por favor...");
                },
                success:  function (response) { 
                        $("#categoria_ajax").html(response);
                }
            });
        }

